<p align="center">
<img src="https://github.com/linux-can/can-logos/raw/master/png/SocketCAN-logo-60dpi.png" alt="SocketCAN logo"/>
<img src="Windows.png" alt="Windows"/>
</p>

## CANX-comp-utils
A cross platform (linux/CentOS and Windows) tool for complex CAN message sending and receiving using USB-CAN or ethernet-CAN bridges. We control sender and receiver message data flows per CAN port in a sophisticated way per thread with a central clock. CAN frames which have a specific meaning on the CAN bus can be configured and sent. Complex testing scenarios can be configured to help with systematic testing of CAN bridges (from systek, anagate, peak) for validation.

see https://readthedocs.web.cern.ch/display/CANDev/CANX-comp-util for user documentation

CERN CC7 is currently (April 2018) at:
'''
Linux version 3.10.0-693.21.1.el7.x86_64 (builder@kbuilder.dev.centos.org) (gcc 
version 4.8.5 20150623 (Red Hat 4.8.5-16) (GCC) ) #1 SMP Wed Mar 7 19:03:37 UTC 
2018
'''

Windows versions are
'''
2008R2
W7
'''


### build instructions
 
The preferred way of satisfying the dependencies is on source-level. 
-Git repositories are needed for CanModule and socketcan. 
-Boost has to be present on the system
Certain Vendor-software (proprietary drivers and libs) is not available for 
source-builds, this software is mentioned specifically for each case.

CANX source dependencies
```
CANX
	CanModule
		(GoogleTest)
		socketcan and proprietary sw
		(LogIt)
		boost
	(LogIt)
	(boost)
```

The sources can be found at
```
CanModule:  git clone https://gitlab.cern.ch/mludwig/CanModule.git
	[ Use a private copy from Ben's branch at
      git clone -b create-stand-alone-CanModule-build https://github.com/quasar-team/CanModule.git 
      with some minor modifications, pulls in also 
      		LogIt 
      		googletest git clone https://github.com/google/googletest.git
     ]
boost:   from boost.org
socketcan: git clone https://gitlab.cern.ch/mludwig/CAN_libsocketcan.git
```

#### CERN CC7 build

tools needed: 
standard C++ buildchain (gcc-c++, cmake) being part of cc7

dependencies: 
socketcan is used for systec 


#### Windows build
2008R2

tools needed:

dependencies:
proprietary libs and drivers from the vendors systec, anagate, peak


### link & run instructions
#### CERN CC7
libs needed: 
    boost 1.59 (recommended), minimum 1.54
	libboost_log.so.1.59.0 
	libboost_log_setup.so.1.59.0
	libboost_filesystem.so.1.59.0
	libboost_program_options.so.1.59.0
	libboost_system.so.1.59.0
	libboost_chrono.so.1.59.0
	libboost_date_time.so.1.59.0
	libboost_thread.so.1.59.0
	libboost_regex.so.1.59.0

libs needed depending on runtime conditions and vendor:
	libsocketcan
