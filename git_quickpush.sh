#!/bin/csh
find ./ -name "CMakeCache.txt" -exec rm {} \;
find ./ -name "CMakeFiles" -exec rm -rf {} \;
rm -rf CMakeFiles
rm -rf html
rm -rf latex
# eliminate 3rd party dependencies on code level (git clones) 
#rm -rf ./LogIt
#rm -rf ./CanModule

# come up with a message for git, no interactive
set MSG=`date`" push from eclipse from "`whoami`"@"`hostname`
echo ${MSG}
git add --all
git commit -m "${MSG}"
git push origin master


