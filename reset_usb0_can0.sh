#!/bin/bash
modprobe can
modprobe can-raw
modprobe can-bcm
modprobe can-dev
lsmod | grep can


ifconfig can0 down
echo "can0 down"
sudo ip link set can0 type can bitrate 250000
ip link set can0 type can restart-ms 100
ifconfig can0 up
echo "can0 up"

