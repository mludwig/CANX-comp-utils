/*
 * SpecialCommands.cpp
 *
 * do some one-time shot stuff directly to CAN bridges. DO NOT use CanModule.
 * this class exists just for labo convenience
 *
 *  Created on: Feb 21, 2019
 *      Author: mludwig
 *
 */

#include "SpecialCommands.h"

#ifdef _WIN32
	#include "AnaGateDllCan.h"
	#include "AnaGateDll.h"
	#include "tchar.h"
	#include "Winsock2.h"
	#include "windows.h"
#else
	#include "AnaGateDLL.h"
	#include "AnaGateDllCan.h"
	typedef unsigned long DWORD;
#endif

namespace SpecialCommands_ns {

SpecialCommands::SpecialCommands() {
	// TODO Auto-generated constructor stub

}

SpecialCommands::~SpecialCommands() {
	// TODO Auto-generated destructor stub
}

AnaInt32 SpecialCommands::anagateSoftwareReset( std::string ip ){
	AnaInt32 timeout = 10000; // 10secs
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
			<< " calling CANRestart timeout= " << timeout << " ms" << std::endl;
	AnaInt32 anaRet = CANRestart( ip.c_str(), timeout );
	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
			<< " CANRestart anaRet= 0x" << std::hex << (int) anaRet << std::dec << std::endl;
	switch( anaRet ){
	case 0x0:{
		std::cout << __FUNCTION__ << " OK CANRestart" << std::endl;
		break;
	}
	case 0x20000:{
		std::cout << __FUNCTION__ << " ERROR CANRestart: EERR_TCPIP_SOCKE: Socket  error  occurred  in  TCP/IP layer." << std::endl;
		break;
	}
	case 0x30000:{
		std::cout << __FUNCTION__ << " ERROR CANRestart: ERR_TCPIP_NOTCONNECTED: Connection to TCP/IP partner can't be established or is disconnected." << std::endl;
		break;
	}
	default:{
		std::cout << __FUNCTION__ << " ERROR CANRestart: 0x" << std::hex << anaRet << std::dec << std::endl;
		break;
	}
	}

	AnaInt32 canModuleHandle;
	anaRet = CANOpenDevice( &canModuleHandle, FALSE, TRUE, 0, ip.c_str(), timeout );
	switch( anaRet ){
	case 0x0:{
		std::cout << __FUNCTION__ << " OK CANOpenDevice" << std::endl;
		break;
	}
	default:{
		std::cout << __FUNCTION__ << " ERROR CANOpenDevice: 0x" << std::hex << anaRet << std::dec << std::endl;
		break;
	}
	}
	AnaInt32 state = CANDeviceConnectState( canModuleHandle );
	std::cout << __FUNCTION__ << " CANDeviceConnectState: device connect state= " << state << std::endl;
	switch ( state ){
	case 1: std::cout << "1 = DISCONNECTED : The connection to the AnaGate is disconnected." << std::endl;  break;
	case 2: std::cout << "2 = CONNECTING : The connection is connecting." << std::endl;  break;
	case 3: std::cout << "3 = CONNECTED : The connection is established." << std::endl;  break;
	case 4: std::cout << "4 = DISCONNECTING : The connection is disconnecting." << std::endl;  break;
	case 5: std::cout << "5 = NOT_INITIALIZED : The network protocol is not successfully initialized." << std::endl;  break;
	}

	anaRet = CANCloseDevice( canModuleHandle );
	std::cout << __FUNCTION__ << " CANCloseDevice: ret= 0x" << std::hex << anaRet << std::dec<< std::endl;

	return anaRet;
}

} /* namespace SpecialCommands_ns */
