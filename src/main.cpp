/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: Wed Aug 30 10:15:57 CEST 2017
 *
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *      Contributors and prior art: Benjamin Farnham, Piotr Nikiel, Viacheslav Filimonov
 *
 * This file is part of the CAN Common Tools project and is the property of CERN, Geneva, Switzerland,
 * and is not free software, since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */

// atlas systec driver modified (&kernel) from:
// atlas CanModule devel version branch: https://github.com/quasar-team/CanModule.git branch CanModule-rev

/** \mainpage CANX-tester
 * \section intro_sec Overview
 * CANX-tester is a cross architectue/vendor/platform tool for CAN testing, for CAN bridges from various vendows.
 * CANX-tester is build and available (bins, libs) for cc7 and all windows platforms.
 * We can send frames in a random, fixed or determined way on many ports in parallel and also receive, record, replay
 * and mirror frames. The primary purpose is to fully test CAN gateways of different vendors systec, anagate, peak, concerning
 * platform, hardware, firmware, driver, api, and their integration into the CERN SW environment.
 *
 * Since the tool can record specific frames on specific ports in a timed way and replay them as well, it can also
 * be used to produce a detailed (=meaningful for CAN bus devices) and synchronized CAN traffic on many ports
 * in parallel, for complex test scenarios.
 *
 *
 * main features:
 *    - Each can port is served by two threads (objects) for sending and receiving
 *    - Create threads=objects for the ports used only, mapping
 *    - send CAN messages in a time controlled way
 *    - receive messages
 *    - mirror messages (=send them) on the same port or a different port
 *    - the traffic patterns are specified in a sequence sheet or on the command line
 *    - detailed diagnostic output: dropped frames sender/receiver, problems, measured frame-rates
 *    - detailed traffic logging to detect electronics problems
 *
 * \section specification_sec Specification and main Documentation
 *  - https://readthedocs.web.cern.ch/display/ICKB/CANCompUtils%253A+UR+specifications
 *  - https://readthedocs.web.cern.ch/display/CANDev/CANX-tester
 *
 *
 * \section usage_sec Usage
 * \subsection sequence_sheet_ec using a sequence-sheet to specify traffic patterns
 *    traffic patterns can be specified in detail with complex timing behavior in a sequence sheet:
 *    		- clock-speed: time interval between two consecutive main clock-ticks
 *    		- module types/vendors
 *    		- usb/ethernet/can ports and their settings for sending
 *    		- usb/ehternet/can ports and their settings for receiving, also mirror to sender ports
 *    		- repeat sequence N times
 *    		- list of clock-ticks and CAN frame(s) to send for each single can port
 *
 * \subsection reports_sec creating test reports
 * Obviously one would like to produce test reports, with all details for future reference
 * in case of trouble. These reports are the logging trace (from LogIt), and they can be
 * summarized using scripts.
 */


#include "main.h"

#include <iostream>

using namespace std;
#ifndef _WIN32
#include <unistd.h>
#endif

#include <CanBusAccess.h>
#include <LogIt.h>

#include "../include/Configuration.h"
#include "../include/connection.h"
#include "../include/SpecialCommands.h"

using namespace connection_ns;
using namespace configuration_ns;

// replaced by parser with version string
#define MYVERSION "CANX Version 1.0.3"

void usage( void ){
	cout << "CANX-tester: send and receive CAN messages on ports," << endl;
	cout << "generate specific multi-threaded scenarios and test sequences." << endl;
	cout << "cross platform, multiple vendors." << endl;
	cout << "USAGE:" << endl;
	cout << " loglevel: [-T,-D,-I,-W,-E] = T(TRC), D(DBG), I(INF), W(WRN), E(ERR)" << endl;
	cout << " configuration: [-cfg <config.xml>]" << endl;
	cout << " special commands: [-anagateSoftwareReset <ip>]" << endl;
	exit(0);
}

CanxThreads::ThreadData_t configureConnection( configuration_ns::Configuration::CFG_THREAD_t ct ){
	CONNECTION::ThreadData_t td;
	td.sender_enabled = ct.sender.enabled;
	td.connection.canPport = ct.canPort;
	td.connection.canLport = -1;                        // logical port used by driver
	td.msgGenerationType = ct.sender.messageGeneration;
	td.nbMsgToGeneratePerTick = ct.sender.generatedNbFramesPerTick;
	td.canIdType = ct.sender.messageStandard;
	td.bitrate = ct.bitrate;

	if ( ct.moduleType == string("systec") ) {
		td.vendor = CanxThreads::Vendor_t::VENDOR_SYSTEC;
		td.connection.usbport = ct.usbPort;
	} else if ( ct.moduleType == string("peak") ) {
		td.vendor = CanxThreads::Vendor_t::VENDOR_PEAK;
		td.connection.usbport = ct.usbPort;
	} else if ( ct.moduleType == string("anagate") ) {
		td.vendor = CanxThreads::Vendor_t::VENDOR_ANAGATE;
		td.anagate.operationalMode = ct.operationMode;
		td.anagate.termination = ct.termination;
		td.anagate.highSpeed = ct.highSpeed;
		td.anagate.timeStamp = ct.timeStamp;
		td.anagate.syncMode = ct.syncMode;
		td.connection.ipNumber = ct.ipNumber;
	} else {
		td.vendor = CanxThreads::Vendor_t::VENDOR_UNKNOWN;
	}

	// receiver
	td.receiver_delay_us = ct.receiver.waitBetweenFrames;
	td.receiverIsMirror = ct.receiver.mirror;
	td.receiverIsRecorder = ct.receiver.recorder;
	td.objIndex = ct.threadIndex;
	td.sender_delay_us = ct.sender.waitBetweenFrames;
	td.receiver_enabled = ct.receiver.enabled;
	td.connection_mirror.canPport = ct.receiver.mirrorCanPort;
	td.connection_mirror.canPport = ct.receiver.mirrorCanPort;
	td.connection_mirror.mirrorIncrementMsgId = ct.receiver.mirrorIncrementMsgId;

	std::ostringstream os;
	os << "sender." << ct.moduleType << "." <<  ct.threadIndex;
	td.threadID = os.str();

	// set up the mirror port as the same port, even if unused for non USB
	td.connection_mirror.usbport = td.connection.usbport;
	td.connection_mirror.canPport = td.connection.canPport;
	td.connection_mirror.canLport = td.connection.canLport;
	LOG(Log::TRC) << __FUNCTION__ << " configured " << td.threadID;
	return( td );
}

CONNECTION::CanBitrates_t convertToBitrate( int rawBitrate ){
	CONNECTION::CanBitrates_t bitrate = CONNECTION::CANBR_125kbs;
	switch( rawBitrate){
	case 10: bitrate = CONNECTION::CANBR_10kbs; break;
	case 20: bitrate = CONNECTION::CANBR_20kbs; break;
	case 50: bitrate = CONNECTION::CANBR_50kbs; break;
	case 125: bitrate = CONNECTION::CANBR_125kbs; break;
	case 250: bitrate = CONNECTION::CANBR_250kbs; break;
	case 500: bitrate = CONNECTION::CANBR_500kbs; break;
	case 800: bitrate = CONNECTION::CANBR_800kbs; break;
	case 1000: bitrate = CONNECTION::CANBR_1000kbs; break;
	default: usage();
	}
	return( bitrate );
}

void clockThread(boost::barrier& cur_barrier, int counter, int tick_ms )
{
	LOG(Log::TRC) << "clockThread: counter= " << counter;
	int cc = counter;
	int tick = tick_ms * 1000;
	while( cc-- ){
		LOG(Log::TRC) << "clock sleeping " << tick_ms << " ms, tick countdown= " << cc;
		CONNECTION::mysleepu( tick );

		// since we have slept while the sender threads were working, we should be the last thread
		// arriving at the barrier. We should therefore never have to wait here. As soon as we have passed
		// the barrier all other sender threads make one tick, since everyone else was waiting already.
		LOG(Log::INF) << "next tick, countdown= " << cc;
		cur_barrier.wait();
	}
}

/**
 * one global declaration without object
 */
void senderThread( boost::barrier& cur_barrier, CONNECTION *sender, int counter, int sequenceRepeat, CanxThreads::ThreadData_t td ){

	int cc = counter;
	int seq = sequenceRepeat + 1;
	sender->initialize( td );

	LOG(Log::TRC) << __FUNCTION__ << " start " << sender->id() << " start tick, counter= " << counter << " repeat= " << sequenceRepeat;
	sender->executeStartTick();

	while ( seq ){

		unsigned int ctick = 0;
		while( cc-- ){
			LOG(Log::TRC) << __FUNCTION__<< " " << sender->id() << " waiting for next tick ";
			cur_barrier.wait();
			LOG(Log::TRC) << __FUNCTION__<< " " << sender->id() << " barrier released, executing tick ";
			sender->executeClockTick( ctick );
			ctick++;
		}

		if ( seq > 1 ){
			sender->resetSequence();
			cc = counter;
		}
		seq--;
		LOG(Log::TRC) << __FUNCTION__ << " " << sender->id() << " repeat sequence another " << seq << " times";
	}
}


int anagateSoftwareReset( string ip ){
	SpecialCommands_ns::SpecialCommands cmd = SpecialCommands_ns::SpecialCommands();
	AnaInt32 ret = cmd.anagateSoftwareReset( ip );
	if ( ret != 0 ) {
		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERROR " << endl;
		return(-1);
	}
	return(0);
}


int main( int argc, char* argv[] )
{
	char special_ip[127];
	int specialFunction = 0;

	Log::LOG_LEVEL loglevel = Log::INF; // default
	bool ret = Log::initializeLogging(loglevel);
	if ( ret ) cout << "CANX: LogIt initialized OK" << endl;
	else cout << "CANX: LogIt problem at initialisation" << endl;
	Log::setNonComponentLogLevel( loglevel );

	LOG(Log::INF) << argv[ 0 ] << MYVERSION << " " << __DATE__ << " " << __TIME__ ;


	if ( argc < 2 ) usage();

	string shortHandSeq;
	Configuration *cfg = new Configuration(); // defaults are assumed
	cfg->setLogLevel( loglevel );
	for ( int i = 1; i < argc; i++ ){
		if ( 0 == strcmp( argv[i], "--help")) usage();
		if ( 0 == strcmp( argv[i], "-h")) usage();

		if ( 0 == strcmp( argv[i], "-cfg")) {
			if ( argc >= i + 1 ) {
				char ff[127];
				sscanf( argv[ i + 1 ], "%s", (char *) &ff );
				string configfile = ff;
				cfg->setLogLevel( loglevel );
				cfg->read( configfile ); // overwrite defaults with specific config
			} else usage();
		}
		if ( 0 == strcmp( argv[i], "-T")) { loglevel = Log::TRC; }
		if ( 0 == strcmp( argv[i], "-D")) { loglevel = Log::DBG; }
		if ( 0 == strcmp( argv[i], "-I")) { loglevel = Log::INF; }
		if ( 0 == strcmp( argv[i], "-W")) { loglevel = Log::WRN; }
		if ( 0 == strcmp( argv[i], "-E")) { loglevel = Log::ERR; }

		if ( 0 == strcmp( argv[i], "-anagateSoftwareReset")) {
			if ( argc >= i + 1 ) {
				sscanf( argv[ i + 1 ], "%s", (char *) &special_ip );
				std::cout << "-anagateSoftwareReset : got special_ip= " << special_ip << endl;
				specialFunction = 1;
			} else {
				std::cout << "-anagateSoftwareReset : missing argument <ip>" << endl;
				usage();
			}
		}
	}
	Log::setNonComponentLogLevel( loglevel );
	std::cout << "Log level set to " << loglevel << endl;

	/**
	 * special functions without using CanModule
	 */
	switch( specialFunction ){
	case 1: {
		// anagate one shot SW reset on the ip number
		anagateSoftwareReset( special_ip );
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " @linux: gives a mem corruption if mod is present" << std::endl;
		return(0);
	}
	default: break;
	}



	/**
	 * one thread per sender per CAN port, with a receiver thread attached
	 */
	typedef std::map<int, CONNECTION *> senderMap_t;
	senderMap_t sender_map;
	vector<CanxThreads::ThreadData_t> sender_initv;
	int nbGlobalTicks = cfg->global().nbTicks;

	/**
	 * set up a barrier for sender thread synchronization. Each sender has it's own thread, and in order to use the
	 * barrier between threads as a central clocking mechanism we need one extra thread, the clock.
	 */
	// https://stackoverflow.com/questions/11407577/how-to-use-boost-barrier#11408125
	boost::barrier bar( cfg->nbThreads() + 1 );
	vector<boost::thread *> threadsv;

	cout << endl << "----prepare threads-----" << endl;


	/**
	 *  prepare the configs and create the objects
	 *  we always instantiate everything, and the disable/enable is just on the level of
	 *  sending and receiving. All objects follow the clock.
	 */
	for ( unsigned int threadNb = 0; threadNb < cfg->nbThreads(); threadNb++ ){
		cout << endl << "----prepare thread" << threadNb << "-----" << endl;

		LOG( Log::DBG ) << "creating config sender" << threadNb
				<< " enabled= " << cfg->thread( threadNb ).sender.enabled;
		CanxThreads::ThreadData_t td_send = configureConnection( cfg->thread( threadNb ) );

		sender_initv.push_back( td_send );
		pair<int, CONNECTION *> sp = make_pair(threadNb, new CONNECTION());
		sender_map.insert( sp );
		sp.second->storeObjPtr();
		sp.second->setLogLevel( loglevel );
		sp.second->setGlobalTickTime( cfg->global().tickTime );

		LOG( Log::DBG ) << "starting thread sender" << threadNb;
#if 0
		threadsv.push_back( new  boost::thread(boost::bind( &senderThread, boost::ref(bar),
				sp.second,
				cfg->global().nbTicks,
				cfg->global().repeat,
				td_send)));
#endif
		boost::thread *t = new  boost::thread(boost::bind( &senderThread, boost::ref(bar),
						sp.second,
						cfg->global().nbTicks,
						cfg->global().repeat,
						td_send));
		// t->detach();
		threadsv.push_back( t );

		CONNECTION::mysleepu( 100000 );
		cout << "----prepare thread" << threadNb << " done-----" << endl;
	}

	/** let all senders reach the first barrier, 1000msec
	 * we could improve this to make sure all can buses are created properly, but that is good enough
	 */
	CONNECTION::mysleepu( 1000000 );
	cout << endl << "----wait for common synchro-----" << endl;

	try {
		/**
		 * load sender configs: we want each sender thread to create its own CanModule Access point
		 * For this we need to push down the configurations to each thread.
		 */
		cout << endl << "----loading configs into senders-----" << endl;
		int nrepeat = cfg->global().repeat;
		LOG( Log::DBG ) << "repeat sequence " << nrepeat << " times in total";
		for ( unsigned int threadNb = 0; threadNb < sender_map.size(); threadNb++){

			CONNECTION *sender = CONNECTION::getObjPtr( threadNb );
			sender->enableSender( cfg->thread( threadNb ).sender.enabled );
			sender->enableReceiver( cfg->thread( threadNb ).receiver.enabled );
			if ( ! cfg->thread( threadNb ).sender.enabled ){
				LOG(Log::WRN) << " disabled sender" << threadNb;
			}
			if ( ! cfg->thread( threadNb ).receiver.enabled ){
				LOG(Log::WRN) << " disabled receiver" << threadNb;
			}
			sender->showConfig();

			// add all configured frames for that tick
			vector<Configuration::CFG_TICK_t> ctickv = cfg->thread( threadNb ).sender.tick_v;
			for ( unsigned int itick = 0; itick < ctickv.size(); itick++ ){
				CONNECTION::CanThreadFrame_t frame;
				frame.tick = ctickv[ itick ].index;
				for ( unsigned int k = 0; k < ctickv[ itick ].frame_v.size(); k++ ){
					frame.canFrame = ctickv[ itick ].frame_v[ k ];
					/*
					 * The sender waits same time between all frames. Lets keep it simple
					 * for now, this can be of course refined further.
					 */
					frame.wait_us = cfg->thread( threadNb ).sender.waitBetweenFrames;
					sender->addFrame( frame );
				}
			}

			sender->setRepeat( nrepeat );
			sender->setGeneratedTicksSender( cfg->global().nbTicks );
			if ( nbGlobalTicks < sender->getTotalTicksSender() ){
				nbGlobalTicks = sender->getTotalTicksSender();
			}
		}
	}
	catch ( std::runtime_error &e ){
		LOG( Log::ERR ) << "launching thread runtime error: " << e.what();
		exit(0);
	}
	catch(std::exception& e)	{
		LOG( Log::ERR  ) << "launching thread standard exception: " << e.what();
		exit(0);
	}
	catch ( ... ){
		LOG( Log::ERR  ) << "launching thread other exception, unknown";
		exit(0);
	}

	cout << endl << "----threads are configured and launched, starting clock ticks in 1000ms----" << endl;
	CanxThreads::mysleepu( 1000000 );

	CanModule::CanStatistics *stats = new CanModule::CanStatistics();
	stats->beginNewRun();

	// start the clock thread
	cout << endl << "----starting clock ticks----" << endl;
	int nbTotalTicks = ( cfg->global().repeat + 1 )* cfg->global().nbTicks;
	threadsv.push_back( new  boost::thread(boost::bind( &clockThread, boost::ref(bar), nbTotalTicks, cfg->global().tickTime )));

	// ... clock ticks are executed in all threads synchonized ....wait for finish
	CanxThreads::mysleepu( 200000 );
	for ( unsigned int it = 0; it < threadsv.size(); it++ ){
		threadsv[ it ]->join();
	}
	CanxThreads::mysleepu( 200000 );

	//cout << endl << "===shut up all senders, finish threads===" << endl;
	//CONNECTION::allSendersShutup();
	int w_ms = 1000000 * sender_map.size();
	LOG( Log::INF ) << "letting receivers consume their buffers (if any) 1000ms per thread = " << w_ms << " ----";
	CanxThreads::mysleepu( w_ms );

	LOG( Log::WRN ) << "OK finished I think, here are your message stats:";
	for ( unsigned int threadNb = 0; threadNb < sender_map.size(); threadNb++){
		CONNECTION *conn = CONNECTION::getObjPtr( threadNb );
		LOG( Log::WRN ) << conn->id() << " total messages sent    = " << conn->totalMessagesSent();
		LOG( Log::WRN ) << conn->id() << " total messages received= " << conn->totalMessagesReceived();
	}

	LOG( Log::INF ) << "OK finished I think, saving any recorded traces and deleting senders, good luck ;-)";

	for ( unsigned int threadNb = 0; threadNb < sender_map.size(); threadNb++){
		CONNECTION *sender = CONNECTION::getObjPtr( threadNb );
		sender->storeRecordingToFile();
		LOG( Log::DBG ) << "deleting sender " << threadNb;
		delete( sender );
	}
	LOG(Log::WRN) << "===CanModule internal statistics===";
	LOG(Log::WRN) << "statistics: total transmitted " << stats->totalTransmitted();
	LOG(Log::WRN) << "statistics: total received    " << stats->totalReceived();
	LOG(Log::WRN) << "statistics: txRate            " << stats->txRate();
	LOG(Log::WRN) << "statistics: rxRate            " << stats->rxRate();
	LOG(Log::WRN) << "statistics: busLoad           " << stats->busLoad();
	LOG(Log::WRN) << "===fertig===";

	return 0;


	/*PEAK Example in windows
	initializeWrapper("PeakCanImplementation");
	char* usb1 = "USB1";

	openCanBus(usb1, parameters);
	const bool result = sendMessage(usb1, 1, 8, (unsigned char*)("12345678"), false);*/

	/*ANAGATE Example
	initializeWrapper("AnagateCanImplementation");
	char* anagate1 = "Ana:1:137.138.37.224";
	char* anagate0 = "Ana:0:137.138.37.224";	
	openCanBus(anagate0, parameters);
	openCanBus(anagate1, parameters);
	connectMessageCameToHandler(anagate0, &onMessageRcvd);
	Sleep(500);
	sendMessage(anagate1, 1, 8, (unsigned char*)("12345678"), false);
	Sleep(1000);
	 */
}
