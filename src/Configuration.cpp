/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: May 7, 2018
 *
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *
 * This file is part of the CAN Common Tools project and is the property of CERN, Geneva, Switzerland,
 * and is not free software, since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */
/**
 * Configuration.cpp
 * class to parse xml table for configuring CANX threads, windows and linux code
 * we are using xerxes which is available for both windows and linux
 */


#include <Configuration.h>

#if _WIN32


#else

#endif

namespace configuration_ns {


Configuration::Configuration() {
	xercesc::XMLPlatformUtils::Initialize();
	parser = new xercesc::XercesDOMParser();
	// parser->setValidationScheme( xercesc::XercesDOMParser::Val_Never );
	parser->setValidationScheme( xercesc::XercesDOMParser::Val_Auto );
	parser->setDoNamespaces( false );
	parser->setDoSchema( false );

	/**
	 * init xml tags
	 */
	_TAG_root = NULL;
	_TAG_global = NULL;
	_TAG_thread = NULL;
	_TAG_sender = NULL;
	_TAG_receiver = NULL;
	_TAG_tick = NULL;
	_TAG_frame = NULL;
	_GLOBAL_ATTR_ticksNumber = NULL;
	_GLOBAL_ATTR_tickTime = NULL;
	_GLOBAL_ATTR_repeat = NULL;
	_THREAD_ATTR_port = NULL;
	_THREAD_ATTR_speed = NULL;
	_SENDER_ATTR_name = NULL;
	_RECEIVER_ATTR_name = NULL;
	_RECEIVER_ATTR_mirror = NULL;
	_RECEIVER_ATTR_mport = NULL;
	_TICK_ATTR_index = NULL;
	_FRAME_value = NULL;


	/**
	 * set basic defaults for the whole CANX to make things work
	 * without extra user input or config file
	 */
	_global.tickTime = 100;
	_global.nbTicks = 1;
	_global.repeat = 0; //    repeat: N=0: run all ticks once, N>0: repeat all ticks N time, N<0: repeat forever</li>

	CFG_TICK_t ctick;
	ctick.index = 0;
	ctick.frame_v.push_back( "12345678" );

	CFG_SENDER_t csender;
	csender.messageGeneration = canxthreads_ns::CanxThreads::MsgGenerationType_t::MSG_COUNTING;
	csender.messageStandard = canxthreads_ns::CanxThreads::CanIDType_t::CANID_STANDARD;
	csender.generatedNbFramesPerTick = 1;
	csender.waitBetweenFrames = 10000; // 10ms
	csender.tick_v.push_back( ctick ); // not needed for counting

	CFG_RECEIVER_t creceiver;
	creceiver.mirror = false;
	creceiver.mirrorUsbPort = -1;
	creceiver.mirrorCanPort = -1;
	creceiver.mirrorIncrementMsgId = false;
	creceiver.recorder = false;

	CFG_THREAD_t cthread;
	cthread.threadIndex = -1;
	cthread.canPort = 0;
	cthread.usbPort = 0;
	cthread.ipNumber = "128.141.159.194"; // anagate default
	cthread.bitrate = canxthreads_ns::CanxThreads::codeString_2_BR("125000");
	cthread.sender = csender;
	cthread.receiver = creceiver;
	_thread_v.push_back( cthread );
}

Configuration::~Configuration() {}

inline bool Configuration::_fileExists( const std::string& name ) {
	if ( FILE *file = fopen(name.c_str(), "r")) {
		fclose(file);
		return true;
	} else {
		return false;
	}
}


void Configuration::read( std::string filename ){
	// if we have an explicit configuration, we clear the simple defaults from the constructor
	_thread_v.clear();

	Configuration::_configfile = filename;
	if ( ! _fileExists( filename )) {
		std::cout << __FILE__ << " " << __LINE__ << " file not found : " << filename << std::endl;
		return;
	}
	try {
		std::cout << __FILE__ << " " << __LINE__ << " Configuration::read parsing " << filename << std::endl;
		parser->parse( filename.c_str() );
	}
	catch (const xercesc::XMLException& toCatch) {
		char* message = xercesc::XMLString::transcode(toCatch.getMessage());
		xercesc::XMLString::release(&message);
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces XML parsing error: " << message << "\n";
		throw std::runtime_error( os.str() );
	}
	catch (const xercesc::DOMException& toCatch) {
		char* message = xercesc::XMLString::transcode(toCatch.msg);
		xercesc::XMLString::release(&message);
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces XML DOM parsing error: " << message << "\n";
		throw std::runtime_error( os.str() );
	}
	catch (...) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces unexpected parsing error ";
		throw std::runtime_error( os.str() );
	}

	std::cout << __FILE__ << " " << __LINE__ << " ...de-serializing " << filename << std::endl;

	xercesc::DOMNode* docRootNode;
	xercesc::DOMDocument* doc;
	doc = parser->getDocument();
	docRootNode = doc->getDocumentElement();
	xercesc::DOMNodeList *rootlist = docRootNode->getChildNodes();

	for ( uint32_t n0 = 0; n0 < rootlist->getLength(); n0++ ){
		char ns0[ NODE_NAME_MAX ] = "";
		xercesc::XMLString::transcode( rootlist->item( n0 )->getNodeName(), ns0, NODE_NAME_MAX );
		// std::cout << __FILE__ << " " << __LINE__ << " level0= " << ns0 << std::endl;

		// global settings
		if ( strcmp(ns0, "global") == 0 ){
			// std::cout << __FILE__ << " " << __LINE__ << " found global " << ns0 << std::endl;

			// global attributes
			xercesc::DOMNamedNodeMap *global_attr = rootlist->item( n0 )->getAttributes();
			std::string s_nbTicks = _getAttributeValue( global_attr, "nbTicks" );
			sscanf( s_nbTicks.c_str(), "%d", &_global.nbTicks );
			_global.nbTicks++; // add one to have count ticks actually played
			LOG(Log::INF) << " global::nbTicks= " << _global.nbTicks;

			std::string s_tickTime = _getAttributeValue( global_attr, "tickTime" );
			sscanf( s_tickTime.c_str(), "%d", &_global.tickTime );
			LOG(Log::INF) << " global::tickTime= " << _global.tickTime;

			std::string s_repeat = _getAttributeValue( global_attr, "repeat" );
			sscanf( s_repeat.c_str(), "%d", &_global.repeat );
			LOG(Log::INF) << " global::repeat= " << _global.repeat;
		}

		// threads
		if ( strcmp(ns0, "thread") == 0 ){
			CFG_THREAD_t configThread;

			LOG(Log::DBG) << " found thread " << ns0;
			// thread attributes
			xercesc::DOMNamedNodeMap *thread_attr = rootlist->item( n0 )->getAttributes();

			// all of them are required
			std::string s_threadIndex = _getAttributeValue( thread_attr, "threadIndex" );
			sscanf( s_threadIndex.c_str(), "%d", &configThread.threadIndex );
			LOG(Log::INF) << " thread::threadIndex= " << configThread.threadIndex;

			std::string s_port = _getAttributeValue( thread_attr, "port" );
			sscanf( s_port.c_str(), "%d", &configThread.canPort );
			LOG(Log::INF) << " thread::port= " << configThread.canPort;

			std::string s_bitrate = _getAttributeValue( thread_attr, "bitrate" );
			configThread.bitrate = canxthreads_ns::CanxThreads::codeString_2_BR( s_bitrate );
			LOG(Log::INF) << " thread::bitrate= " << configThread.bitrate << " (typed)";

			configThread.moduleType = _getAttributeValue( thread_attr, "moduleType" );
			LOG(Log::INF) << " thread::moduleType= " << configThread.moduleType;

			// --- optional depending on vendor
			// systec
			std::string s_usb = _getAttributeValue( thread_attr, "st_usb" );// required
			sscanf( s_usb.c_str(), "%d", &configThread.usbPort );
			LOG(Log::INF) << " thread::usb= " << configThread.usbPort;

			// anagate
			configThread.ipNumber = _getAttributeValue( thread_attr, "ana_ip" ); // required
			LOG(Log::INF) << " thread::ip= " << configThread.ipNumber;

			std::string s_opmode =_getAttributeValue( thread_attr, "ana_operationMode" ); // optional
			sscanf( s_opmode.c_str(), "%d", &configThread.operationMode );
			LOG(Log::INF) << " thread::operationMode= " << configThread.operationMode;

			std::string s_termination = _getAttributeValue( thread_attr, "ana_termination" );// optional
			sscanf( s_termination.c_str(), "%d", &configThread.termination );
			LOG(Log::INF) << " thread::termination= " << configThread.termination;

			std::string s_highSpeed = _getAttributeValue( thread_attr, "ana_highSpeed" );// optional
			sscanf( s_highSpeed.c_str(), "%d", &configThread.highSpeed );
			LOG(Log::INF) << " thread::highSpeed= " << configThread.highSpeed;

			std::string s_timeStamp = _getAttributeValue( thread_attr, "ana_timeStamp" );// optional
			sscanf( s_timeStamp.c_str(), "%d", &configThread.timeStamp );
			LOG(Log::INF) << " thread::timeStamp= " << configThread.timeStamp;

			std::string s_syncMode = _getAttributeValue( thread_attr, "ana_syncMode" );// optional
			sscanf( s_syncMode.c_str(), "%d", &configThread.syncMode );
			LOG(Log::INF) << " thread::syncMode= " << configThread.syncMode;

			// ---

			xercesc::DOMNodeList *threads = rootlist->item( n0 )->getChildNodes();
			for ( uint32_t n1 = 0; n1 < threads->getLength(); n1++ ){
				char ns1[ NODE_NAME_MAX ] = "";
				xercesc::XMLString::transcode( threads->item( n1 )->getNodeName(), ns1, NODE_NAME_MAX );
				//std::cout << __FILE__ << " " << __LINE__ << " level1= " << ns1 << std::endl;

				// sender
				if ( strcmp(ns1, "sender") == 0 ){
					CFG_SENDER_t configSender;
					// std::cout << __FILE__ << " " << __LINE__ << " found sender " << ns1 << std::endl;
					// sender attributes
					xercesc::DOMNamedNodeMap *sender_attr = threads->item( n1 )->getAttributes();

					std::string s_generatedNbFramesPerTick = _getAttributeValue( sender_attr, "generatedNbFramesPerTick" );
					sscanf( s_generatedNbFramesPerTick.c_str(), "%d", &configSender.generatedNbFramesPerTick );
					LOG(Log::INF) << "\t" << "sender::generatedNbFramesPerTick= " << configSender.generatedNbFramesPerTick;

					std::string s_enabled = _getAttributeValue( sender_attr, "enabled" );
					if ( s_enabled == "true" ) configSender.enabled = true;
					else configSender.enabled = false;
					LOG(Log::INF) << "\t" << "sender::enabled= " << configSender.enabled;

					std::string s_waitBFrames = _getAttributeValue( sender_attr, "waitBetweenFrames" );
					sscanf( s_waitBFrames.c_str(), "%d", &configSender.waitBetweenFrames );
					LOG(Log::INF) << "\t" << "sender::waitBetweenFrames= " << configSender.waitBetweenFrames;

					std::string s_messageStandard = _getAttributeValue( sender_attr, "messageStandard" );
					if ( s_messageStandard == "standard" ) configSender.messageStandard = canxthreads_ns::CanxThreads::CanIDType_t::CANID_STANDARD;
					if ( s_messageStandard == "extended" ) configSender.messageStandard = canxthreads_ns::CanxThreads::CanIDType_t::CANID_EXTENDED;
					LOG(Log::INF) << "\t" << "sender::messageStandard= " << configSender.messageStandard;

					std::string s_messageGeneration = _getAttributeValue( sender_attr, "messageGeneration" );
					if ( s_messageGeneration == "random" ) configSender.messageGeneration = canxthreads_ns::CanxThreads::MsgGenerationType_t::MSG_RANDOM;
					if ( s_messageGeneration == "counting" ) configSender.messageGeneration = canxthreads_ns::CanxThreads::MsgGenerationType_t::MSG_COUNTING;
					if ( s_messageGeneration == "fixed" ) configSender.messageGeneration = canxthreads_ns::CanxThreads::MsgGenerationType_t::MSG_FIXED;
					if ( s_messageGeneration == "asConfigured" ) configSender.messageGeneration = canxthreads_ns::CanxThreads::MsgGenerationType_t::MSG_ASCONFIGURED;
					if ( s_messageGeneration == "unused" ) configSender.messageGeneration = canxthreads_ns::CanxThreads::MsgGenerationType_t::MSG_UNUSED;
					LOG(Log::INF) << "\t" << "sender::messageGeneration= " << configSender.messageGeneration;

					if ( configSender.messageGeneration == canxthreads_ns::CanxThreads::MsgGenerationType_t::MSG_ASCONFIGURED ) {

						for ( uint32_t n2 = 0; n2 < threads->getLength(); n2++ ){
							char ns2[ NODE_NAME_MAX ] = "";
							xercesc::XMLString::transcode( threads->item( n2 )->getNodeName(), ns2, NODE_NAME_MAX );
							//std::cout << __FILE__ << " " << __LINE__ << " level2= " << ns2 << std::endl;


							// tick
							xercesc::DOMNodeList *senders = threads->item( n2 )->getChildNodes();
							for ( uint32_t n3 = 0; n3 < senders->getLength(); n3++ ){
								char ns3[ NODE_NAME_MAX ] = "";
								xercesc::XMLString::transcode( senders->item( n3 )->getNodeName(), ns3, NODE_NAME_MAX );
								//std::cout << __FILE__ << " " << __LINE__ << " level3= " << ns3 << std::endl;

								if ( strcmp(ns3, "tick") == 0 ){
									CFG_TICK_t configTick;
									// std::cout << __FILE__ << " " << __LINE__ << " found tick " << ns3 << std::endl;
									// tick attributes
									xercesc::DOMNamedNodeMap *tick_attr = senders->item( n3 )->getAttributes();
									std::string s_index = _getAttributeValue( tick_attr, "index" );
									sscanf( s_index.c_str(), "%d", &configTick.index );
									LOG(Log::INF) << "\t\t" << "tick::index= " << configTick.index;

									xercesc::DOMNodeList *ticks = senders->item( n3 )->getChildNodes();
									for ( uint32_t n4 = 0; n4 < ticks->getLength(); n4++ ){
										char ns4[ NODE_NAME_MAX ] = "";
										xercesc::XMLString::transcode( ticks->item( n4 )->getNodeName(), ns4, NODE_NAME_MAX );
										//std::cout << __FILE__ << " " << __LINE__ << " level4= " << ns4 << std::endl;

										// frames
										if ( strcmp(ns4, "frame") == 0 ){
											// std::cout << __FILE__ << " " << __LINE__ << " found frame " << ns4 << std::endl;
											// frame value ...
											char ns4_value[ NODE_NAME_MAX ] = "";
											// xercesc::XMLString::transcode( ticks->item( n4 )->getNodeValue(), ns4_value, NODE_NAME_MAX );
											xercesc::XMLString::transcode( ticks->item( n4 )->getTextContent(), ns4_value, NODE_NAME_MAX );
												if ( string(ns4_value).length() < FRAME_STANDARD_LENGTH ){
												LOG(Log::ERR) << "\t\t\t" << ns4 << "::value= " << ns4_value << " must have "
														<< FRAME_STANDARD_LENGTH << " chars [ID.RTR.IDE.R0.DLC.DATA.CRC.ACK], skipping frame";
											} else {
												configTick.frame_v.push_back( ns4_value );
												LOG(Log::INF) << "\t\t\t" << ns4 << "::value= " << ns4_value << " OK";
											}
										}
									}
									configSender.tick_v.push_back( configTick );
								}
							}
						} // if message generation is configured
					}
					configThread.sender = configSender;
				}

				// receiver
				if ( strcmp(ns1, "receiver") == 0 ){
					CFG_RECEIVER_t configReceiver;
					//std::cout << __FILE__ << " " << __LINE__ << " found receiver " << ns1 << std::endl;
					// receiver attributes
					xercesc::DOMNamedNodeMap *receiver_attr = threads->item( n1 )->getAttributes();

					std::string s_enabled = _getAttributeValue( receiver_attr, "enabled" );
					if ( s_enabled == "true" ) configReceiver.enabled = true;
					else configReceiver.enabled = false;
					LOG(Log::INF) << "\t" << "receiver::enabled= " << configReceiver.enabled;

					std::string s_mirror = _getAttributeValue( receiver_attr, "mirror" );
					if ( s_mirror == "true" ) configReceiver.mirror = true;
					else configReceiver.mirror = false;
					LOG(Log::INF) << "\t" << "receiver::mirror= " << configReceiver.mirror;

					std::string s_increment = _getAttributeValue( receiver_attr, "mirrorIncrementMsgId" );
					if ( s_increment == "true" ) configReceiver.mirrorIncrementMsgId = true;
					else configReceiver.mirrorIncrementMsgId = false;
					LOG(Log::INF) << "\t" << "receiver::mirrorIncrementMsgId= " << configReceiver.mirrorIncrementMsgId;

					std::string s_recorder = _getAttributeValue( receiver_attr, "recorder" );
					if ( s_recorder == "true" ) configReceiver.recorder = true;
					else configReceiver.recorder = false;
					LOG(Log::INF) << "\t" << "receiver::recorder= " << configReceiver.recorder;

					std::string s_mirrorusbport = _getAttributeValue( receiver_attr, "mirrorusbport" );
					sscanf( s_mirrorusbport.c_str(), "%d", &configReceiver.mirrorUsbPort );
					LOG(Log::INF) << "\t" << "receiver::mirrorusbport= " << configReceiver.mirrorUsbPort;

					std::string s_mirrorcanport = _getAttributeValue( receiver_attr, "mirrorcanport" );
					sscanf( s_mirrorcanport.c_str(), "%d", &configReceiver.mirrorCanPort );
					LOG(Log::INF) << "\t" << "receiver::mirrorcanport= " << configReceiver.mirrorCanPort;

					std::string s_waitBetweenFrames = _getAttributeValue( receiver_attr, "waitBetweenFrames" );
					sscanf( s_waitBetweenFrames.c_str(), "%d", &configReceiver.waitBetweenFrames );
					LOG(Log::INF) << "\t" << "receiver::waitBetweenFrames= " << configReceiver.waitBetweenFrames;

					configThread.receiver = configReceiver;
				}
			}
			_thread_v.push_back( configThread );
		}
	}
	LOG(Log::INF) << " finished reading= " << filename;
	LOG(Log::INF) << " have " << _thread_v.size() << " threads ";
}

std::string Configuration::_getAttributeValue( xercesc::DOMNamedNodeMap *attributes, std::string attributeName ){
	for ( uint32_t a0 = 0; a0 < attributes->getLength(); a0++ ){
		char nodeName[ NODE_NAME_MAX ] = "";
		char stringValue[ NODE_NAME_MAX ] = "";
		xercesc::XMLString::transcode( attributes->item( a0 )->getNodeName(), nodeName, NODE_NAME_MAX );
		if ( strcmp( nodeName, attributeName.c_str() ) == 0 ) {
			xercesc::XMLString::transcode( attributes->item( a0 )->getTextContent(), stringValue, NODE_NAME_MAX );
			return( std::string( stringValue ));
		}
	}
	return("attribute " + attributeName + " not found");
}


} /* namespace configuration_ns */
