/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: Wed Aug 30 10:15:57 CEST 2017
 *
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *      Contributors and prior art: Benjamin Farnham, Piotr Nikiel, Viacheslav Filimonov
 *
 * This file is part of the CAN Common Tools project and is the property of CERN, Geneva, Switzerland,
 * and is not free software, since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */
/*
 * sender.cpp
 *
 *  Created on: Aug 11, 2017
 *      Author: mludwig
 *
 *  sends CAN messages:
 *  foreground single messaging
 *  background thread message pattern according to established syntax
 *  background message protocol sending from file: free syntax, timestamps
 */

#include "../include/connection.h"

using namespace std;
using namespace canxthreads_ns;
using namespace CanModule;


namespace connection_ns {

/* static */ // int SENDER::_totalMessageCounter = 0;
/* static */ int CONNECTION::_instanceCounter = 0;
/* static */ vector<CONNECTION *> CONNECTION::objSenderv;
/* static */ bool CONNECTION::shutup = false;


CONNECTION::CONNECTION( void )  {
	_timeoutValue = 0;
	_delta = 0;
	_measDelay = 0;
	_clock = -1;
	_previousClock = -2;
	_message = "";
	_delay_us = 1000;
	_thread_frames_index = 0;
	_frameCount = 0;
	_instance = CONNECTION::_instanceCounter;
	_nbGeneratedTicksToExecute = 0;
	_cca = NULL;
	_ccaMirror = NULL;
	_libloader = NULL;
	_repeatSequence = false;
	_messageCounterSequence = 0;
	_messageCounterClockTick = 0;
	_globalTickTime = 0;
	time_t now = time(NULL);
	srand((int) now );
	_sender_enabled = true;
	_receiver_enabled = true;
	_totalMessagesSent = 0;
	_totalMessagesReceived = 0;
	CONNECTION::_instanceCounter++;
}

CONNECTION::~CONNECTION() {
	LOG( Log::DBG ) << "sender: closing " << threadData.objIndex;
	if ( _cca != 0 ) {
		delete( _cca );
	}
}

/* static */ CONNECTION * CONNECTION::getObjPtr( unsigned int i ){
	if ( i < CONNECTION::objSenderv.size( )) {
		return( CONNECTION::objSenderv[ i ]);
	} else {
	 	LOG( Log::ERR ) << "getObjPtr: object " << i << " not found ";
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " getObjPtr: object " << i << " not found " << endl;
		throw std::runtime_error( os.str() );
	}
}

void CONNECTION::storeObjPtr( void ){
	CONNECTION::objSenderv.push_back( this );
	LOG( Log::DBG ) << " storeObjPtr size= " << CONNECTION::objSenderv.size();
}


void CONNECTION::showConfig( void ){
	std::ostringstream os;
	os << threadData.threadID << " SENDER CONFIGURATION "<< endl
			<< " sender enabled " << threadData.sender_enabled << endl
			<< " receiver enabled " << threadData.receiver_enabled << endl
			<< " Pport " << threadData.connection.canPport << endl
			<< " bitrate " << threadData.sbitrate << endl
			<< " recording " << threadData.receiverIsRecorder<< endl
			<< " mirror " << threadData.receiverIsMirror<< endl
			<< " mirrorIncrementMsgId " << threadData.connection_mirror.mirrorIncrementMsgId << endl
			<< " delay between sent successive msg " << threadData.sender_delay_us<< endl
			<< " receiver delay " << threadData.receiver_delay_us << endl;

	switch ( threadData.vendor ){
	case VENDOR_ANAGATE:{
		os << " anagate" << endl
				<< " ip " << threadData.connection.ipNumber << endl
				<< " highSpeed " << threadData.anagate.highSpeed << endl
				<< " operationalMode " << threadData.anagate.operationalMode << endl
				<< " syncMode " << threadData.anagate.syncMode << endl
				<< " termination " << threadData.anagate.termination << endl
				<< " timeStamp " << threadData.anagate.timeStamp << endl;
		break;
	}
	case VENDOR_SYSTEC :{
		os << " systec" << endl
				<< " usbport " << threadData.connection.usbport << endl
				<< " socket " << threadData.connection.socketName << endl
				<< " mirror socket " << threadData.connection_mirror.socketName << endl;
		break;
	}
	default :{
		os << "VENDOR UNKNOWN";
		break;
	}
	}
	LOG( Log::INF ) << os.str() << endl;
}


/* static */ void CONNECTION::onMessageRcvdCommon( uint32_t iobj, const CanMsgStruct/*&*/ message ){
	CONNECTION *rec = CONNECTION::objSenderv[ iobj ];
	LOG( Log::TRC ) << __FUNCTION__ << " id= " << rec->id()	<< " enabled= " <<  rec->receiverEnabled();
	if ( !rec->receiverEnabled() ) return;
	rec->_processReceivedMessage( message );
}
/**
 * produce a nicely formatted string to actually see the message properly
 *
 * the frame.data specified is actually the whole can message, including ID, flags etc etc.
 * The frame is a string, with dots as delimiters:
 * ID.RTR.IDE.R0.DLC.DATA.CRC.ACK
 * ID  = 3 chars representing hex range 000...7ff for ID, 11 bit
 * RTR = 1 char representing a boolean RTR, 1 bit
 * IDE = 1 char representing a boolean IDE, 1 bit
 * R0  = 1 char representing a boolean R0, 1 bit
 * DLC = 1 char representing hex range 0...f, 4 bit
 * DATA = 16 chars representing in hex 8 byte, 64bit
 * CRC = 4 chars representing hex range 0000...ffff, 16 bit
 * ACK = 1 char for 1 bit
 * =====
 * makes 28 chars in the string, plus the 7 dots = 35
 *
 * SOF, EOF and IFS not specified
 */
string CONNECTION::_CanMsgToString( const CanMsgStruct m ){
	stringstream ss;
	ss << hex << setfill('0') << setw(3) << (int) m.c_id; // standard message 11 bits id
	ss << "." << m.c_rtr << "."<< (int) m.c_ff <<"." << (int) m.c_dlc << ".";
	for ( unsigned int i = 0; i < 8; i++ ){
		ss << hex << setfill('0') << setw(2) << (int) m.c_data[ i ];
	}
	ss << ".xxxx.x";
	return( ss.str() );
}


void CONNECTION::_processReceivedMessage( const CanMsgStruct message ){
	if ( !_receiver_enabled ) {
		LOG( Log::TRC ) << "(disabled RECEIVE FRAME: " << threadData.threadID << ")";
		return;
	}
	LOG( Log::INF ) << "RECEIVE FRAME: " << threadData.threadID
			<< "] frame= [" << _CanMsgToString( message )
			<< "] time [" << message.c_time.tv_sec << "." << message.c_time.tv_usec << "]";
	_totalMessagesReceived++;

	// mirror message
	if ( threadData.receiverIsMirror ){
		CanMsgStruct mmessage =  message;
		if ( threadData.connection_mirror.mirrorIncrementMsgId ){
			mmessage.c_id++;
		}
		_ccaMirror->sendMessage( (CanMessage *) &mmessage );
		LOG( Log::INF  ) << "SEND MIRROR FRAME: " << threadData.threadID
				<< "] frame= [" << _CanMsgToString( mmessage )
				<< " to " << threadData.connection_mirror.socketName;
	}
	// record it if needed
	if ( threadData.receiverIsRecorder ){
		_rec.can = message;
		{
			ostringstream convert;
			convert << message.c_time.tv_sec << "." << message.c_time.tv_usec;
			_rec.time = convert.str();
		}
		_recording.push_back( _rec );
	}
	// slow down if needed until ready for more on this port
	CanxThreads::mysleepu( threadData.receiver_delay_us );
}


/**
 * this is just a shorthand way to produce many separate handler functions, since we need an extra instance for each thread.
 */
/* static */ void CONNECTION::onMessageRcvd0(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 0, message );}
/* static */ void CONNECTION::onMessageRcvd1(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 1, message );}
/* static */ void CONNECTION::onMessageRcvd2(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 2, message );}
/* static */ void CONNECTION::onMessageRcvd3(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 3, message );}
/* static */ void CONNECTION::onMessageRcvd4(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 4, message );}
/* static */ void CONNECTION::onMessageRcvd5(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 5, message );}
/* static */ void CONNECTION::onMessageRcvd6(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 6, message );}
/* static */ void CONNECTION::onMessageRcvd7(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 7, message );}
/* static */ void CONNECTION::onMessageRcvd8(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 8, message );}
/* static */ void CONNECTION::onMessageRcvd9(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 9, message );}
/* static */ void CONNECTION::onMessageRcvd10(const CanMsgStruct/*&*/ message){	CONNECTION::onMessageRcvdCommon( 10, message );}
/* static */ void CONNECTION::onMessageRcvd11(const CanMsgStruct/*&*/ message){	CONNECTION::onMessageRcvdCommon( 11, message );}
/* static */ void CONNECTION::onMessageRcvd12(const CanMsgStruct/*&*/ message){	CONNECTION::onMessageRcvdCommon( 12, message );}
/* static */ void CONNECTION::onMessageRcvd13(const CanMsgStruct/*&*/ message){	CONNECTION::onMessageRcvdCommon( 13, message );}
/* static */ void CONNECTION::onMessageRcvd14(const CanMsgStruct/*&*/ message){	CONNECTION::onMessageRcvdCommon( 14, message );}
/* static */ void CONNECTION::onMessageRcvd15(const CanMsgStruct/*&*/ message){ CONNECTION::onMessageRcvdCommon( 15, message );}

void CONNECTION::storeRecordingToFile( void ){
	if ( ! threadData.receiverIsRecorder ) {
		LOG( Log::TRC ) << threadData.threadID << " no recording to store";
		return;
	}
	string filename = string("recording.") + threadData.threadID + string(".txt");
	LOG( Log::INF ) << threadData.threadID << " store recording to " << filename;
	std::ofstream fout( filename );
	time_t now = time(NULL);
	fout << "# " << ctime(&now);
	fout << "# time \t canFrame: id.rtr.ide.dlc.data.crc.ack" << endl;
	for ( unsigned int i = 0; i < _recording.size(); i++){
		fout << _recording[ i ].time << " \t" << _stringFromCanMsgStruct( _recording[ i ].can ) << endl;
	}
}

/**
 * the frame.data specified is actually the whole can message, including ID, flags etc etc.
 * The frame is a string, with dots as delimiters:
 * ID.RTR.IDE.R0.DLC.DATA.CRC.ACK
 * ID  = 3 chars representing hex range 000...7ff for ID, 11 bit
 * RTR = 1 char representing a boolean RTR, 1 bit
 * IDE = 1 char representing a boolean IDE, 1 bit
 * R0  = 1 char representing a boolean R0, 1 bit
 * DLC = 1 char representing hex range 0...f, 4 bit
 * DATA = 16 chars representing in hex 8 byte, 64bit
 * CRC = 4 chars representing hex range 0000...ffff, 16 bit
 * ACK = 1 char for 1 bit
 * =====
 * makes 28 chars in the string, plus the 7 dots = 35
 *
 * SOF, EOF and IFS not specified
 */
string CONNECTION::_stringFromCanMsgStruct( CanMsgStruct &cm ){
	ostringstream convert;
	ostringstream convert0;
	for ( int i = 0; i < cm.c_dlc; i++ ){
		convert0 << setw( 2 ) << setfill('0') << hex << (int) cm.c_data[ i ];
	}
	convert << hex << setfill('0') << setw( 3 ) << cm.c_id << "."
			<< setw( 1 ) << cm.c_rtr << "."
			<< "x." // ide
			<< setw( 3 ) << (int) cm.c_dlc << "."
			<< convert0.str()
			<< ".xxxx.x"; // crc & ack
	return( convert.str() );
}

/**
 * set up the object/thread to be ready for starting work. The connection is vendor specific
 */
void CONNECTION::initialize( CONNECTION::ThreadData_t td ){
	clearFrames();
	_messageCounterSequence = 0;
	threadData = td;
	_sender_enabled = td.sender_enabled;
	_receiver_enabled = td.receiver_enabled;

	LOG( Log::TRC ) <<  __FUNCTION__ << "  initialize enabled= " << threadData.sender_enabled;
	threadData.sbitrate = codeBR_2_string( threadData.bitrate );
	switch ( td.vendor ) {
	case Vendor_t::VENDOR_SYSTEC:{
		LOG( Log::TRC ) << "SENDER::initialize vendor= systec instance " << _instance;
		threadData.connection.socketName = getSystecSocketName( threadData.connection.usbport, threadData.connection.canPport );
		LOG( Log::INF ) << threadData.threadID << " initializing CAN " << threadData.connection.socketName.c_str() << " bitrate " << threadData.sbitrate;
		_initCANSystec( threadData.connection.socketName, threadData.sbitrate );
		break;
	}
	case Vendor_t::VENDOR_PEAK:{
		LOG( Log::TRC ) << "SENDER::initialize vendor= peak instance " << _instance;
		threadData.connection.socketName = getPeakSocketName( threadData.connection.usbport, threadData.connection.canPport );
		LOG( Log::INF ) << threadData.threadID << " initializing CAN " << threadData.connection.socketName.c_str() << " bitrate " << threadData.sbitrate;
		_initCANPeak( threadData.connection.socketName, threadData.sbitrate );
		break;
	}
	case Vendor_t::VENDOR_ANAGATE:{
		LOG( Log::TRC ) << "SENDER::initialize vendor= anagate instance " << _instance;;
		threadData.connection.ipNumber = getAnagateIpNumber( threadData );

		// parameters look like 250000 0 1 0 0 1
		string parameters = threadData.sbitrate
				+ " " + std::to_string( threadData.anagate.operationalMode )
		+ " " + std::to_string( threadData.anagate.termination )
		+ " " + std::to_string( threadData.anagate.highSpeed )
		+ " " + std::to_string( threadData.anagate.timeStamp )
		+ " " + std::to_string( threadData.anagate.syncMode );

		// name looks like: an:0:128.141.159.194
		string portName = string("an:") + getAnagatePport( threadData ) + string(":") + getAnagateIpNumber( threadData );
		LOG( Log::DBG ) << threadData.threadID << " initializing anaGate CAN port= " << portName << " parameters= " << parameters;
		_initCANAnagate(portName, parameters);
		break;
	}
	default:{
		LOG( Log::ERR ) << threadData.threadID << " initializing CAN: vendor unknown ";
		exit(-1);
	}
	}

	/** connect a handler to the socketCan wrapper
	 * we want each CAN port being listened to by its own separate receiver-thread, and each receiver is
	 * an object of class RECEIVER. We want to use objects for subsequent data treatment.
	 * Static methods are obligatory for this callback but this would create
	 * a threading bottleneck and corruption, so lets connect a different static handler for each object.
	 * Can have 16 at max, though ;-)
	 * Otherwise the wrapper would just call the same static global function each time one of the can ports
	 * receives something, but then we could write C-code straight as well, not funny. We want multi-threaded and C++.
	 */
	// have to connect a different handler "instance" for each thread

	if ( _receiver_enabled ) {
		LOG( Log::DBG  ) << __FUNCTION__ << " " << threadData.threadID << " connecting receiver handler instance= " << _instance;

		switch (_instance){
		case 0: { _cca->canMessageCame.connect( &onMessageRcvd0 ); break; }
		case 1: { _cca->canMessageCame.connect( &onMessageRcvd1 ); break; }
		case 2: { _cca->canMessageCame.connect( &onMessageRcvd2 ); break; }
		case 3: { _cca->canMessageCame.connect( &onMessageRcvd3 ); break; }
		case 4: { _cca->canMessageCame.connect( &onMessageRcvd4 ); break; }
		case 5: { _cca->canMessageCame.connect( &onMessageRcvd5 ); break; }
		case 6: { _cca->canMessageCame.connect( &onMessageRcvd6 ); break; }
		case 7: { _cca->canMessageCame.connect( &onMessageRcvd7 ); break; }
		case 8: { _cca->canMessageCame.connect( &onMessageRcvd8 ); break; }
		case 9: { _cca->canMessageCame.connect( &onMessageRcvd9 ); break; }
		case 10: { _cca->canMessageCame.connect( &onMessageRcvd10 ); break; }
		case 11: { _cca->canMessageCame.connect( &onMessageRcvd11 ); break; }
		case 12: { _cca->canMessageCame.connect( &onMessageRcvd12 ); break; }
		case 13: { _cca->canMessageCame.connect( &onMessageRcvd13 ); break; }
		case 14: { _cca->canMessageCame.connect( &onMessageRcvd14 ); break; }
		case 15: { _cca->canMessageCame.connect( &onMessageRcvd15 ); break; }
		default: {
			LOG( Log::ERR ) << "max number of receiver threads (16) exceeded";
			mysleepu( 2000000 );
			exit(-1);
		}
		}
		LOG( Log::INF ) << threadData.threadID << " OK initializing, connected receiver handler instance= "	<< _instance;
	} else {
		LOG( Log::WRN ) << threadData.threadID << " receiver disabled, not connecting handler instance= " << _instance;
	}
}

void CONNECTION::_measureDelayStart( void ){
	_measDelay = 0;
#ifdef _WIN32
	GetSystemTime(&_dstart);
#else
	gettimeofday( &_dstart, &_tz);
#endif
}

double CONNECTION::_measureDelayStop( void ){
#ifdef _WIN32
	GetSystemTime(&_dstop);
	_measDelay = (_dstop.wSecond * 1000) + _dstop.wMilliseconds - (_dstart.wSecond * 1000) - _dstart.wMilliseconds;
#else
	gettimeofday( &_dstop, &_tz);
	_measDelay = (double) ( _dstop.tv_sec - _dstart.tv_sec) * 1000 + (double) ( _dstop.tv_usec - _dstart.tv_usec) / 1000.0;
#endif
	return ( _measDelay );
}


bool CONNECTION::_timeout( void ){
#ifdef _WIN32
	GetSystemTime(&_now);
	_delta = (_now.wSecond * 1000) + _now.wMilliseconds - (_t1.wSecond * 1000) - _t1.wMilliseconds;
#else
	gettimeofday( &_now, &_tz);
	_delta = (double) ( _now.tv_sec - _t1.tv_sec) * 1000 + (double) ( _now.tv_usec - _t1.tv_usec) / 1000.0;
#endif
	if ( _delta > _timeoutValue ) {
		LOG( Log::WRN ) << "timeout " << _timeoutValue << " reached after " << _delta << " ms ! ";
		return( true );
	}
	else {
		return( false );
	}
}
bool CONNECTION::_timeout( double tt_ms ){ // reset
#ifdef _WIN32
	GetSystemTime(&_now);
#else
	gettimeofday( &_now, &_tz);
#endif
	_timeoutValue = tt_ms;
	_t1 = _now;
	_delta = 0;
	LOG( Log::INF ) << "timeout set to " << _timeoutValue << " ms.";
	return( true );
}


void CONNECTION::_initCANSystec( string port, string parameters ){
	string systecLibName;
#ifdef _WIN32
	parameters = "Unspecified"; // seems not working with explicit bitrate
	systecLibName = "st";
#else
	systecLibName = "sock";
#endif
	LOG( Log::TRC ) << __FUNCTION__ << " starting to load/open/create CAN bus at port [" << port << "] parameters [" << parameters << "]" << " lib [" << systecLibName << "]";
	if ( _libloader == NULL ){
		LOG( Log::TRC ) << __FUNCTION__<< " loading lib " << systecLibName;
		_libloader = CanModule::CanLibLoader::createInstance( systecLibName );
	} else {
		LOG( Log::TRC ) << __FUNCTION__<< " lib " << systecLibName << " is loaded already";
	}
	_cca = _libloader->openCanBus( port, parameters );
	if (_cca != NULL) {
		bool ret = _cca->createBus( port, parameters );
		if ( ret ) {
			LOG( Log::TRC ) << __FUNCTION__<< " open CAN port [" << port << "] _parameters [" << parameters << "]" << " lib [" << systecLibName << "] OK";
		} else {
			LOG( Log::ERR ) << __FUNCTION__<< " open CAN port [" << port << "] _parameters [" << parameters << "]" << " lib [" << systecLibName << "]";
		}
	} else {
		LOG( Log::TRC ) << __FUNCTION__ << " cca " << _cca->getBusName() << " is created already";
	}
	LOG( Log::DBG ) << "CAN access bus name= " << _cca->getBusName();

	if ( threadData.receiverIsMirror ){
		LOG( Log::DBG ) << __FUNCTION__<< " CAN mirror port= " << threadData.connection_mirror.canPport
				<< " usbport= " << threadData.connection_mirror.usbport;
		threadData.connection_mirror.socketName = getSystecSocketName( threadData.connection_mirror.usbport, threadData.connection_mirror.canPport );
		_ccaMirror = _libloader->openCanBus( threadData.connection_mirror.socketName, parameters);
		LOG( Log::DBG ) << __FUNCTION__<< " CAN mirror access bus name= " << _ccaMirror->getBusName();
	}
	CanxThreads::addOpenPort( port );
}


void CONNECTION::_initCANPeak( string port, string parameters ){
	string peakLibName;
#ifdef _WIN32
	parameters = "Unspecified"; // seems not working with explicit bitrate
	peakLibName = "pk";
#else
	peakLibName = "pk";
#endif
	LOG( Log::TRC ) << __FUNCTION__ << " starting to load/open/create CAN bus at port [" << port << "] parameters [" << parameters << "]" << " lib [" << peakLibName << "]";
	if ( _libloader == NULL ){
		LOG( Log::TRC ) << __FUNCTION__<< " loading lib " << peakLibName;
		_libloader = CanModule::CanLibLoader::createInstance( peakLibName );
	} else {
		LOG( Log::TRC ) << __FUNCTION__<< " lib " << peakLibName << " is loaded already";
	}
	_cca = _libloader->openCanBus( port, parameters );
	if (_cca != NULL) {
		bool ret = _cca->createBus( port.c_str(), parameters.c_str() );
		if ( ret ) {
			LOG( Log::TRC ) << __FUNCTION__<< " open CAN port [" << port << "] _parameters [" << parameters << "]" << " lib [" << peakLibName << "] OK";
		} else {
			LOG( Log::ERR ) << __FUNCTION__<< " open CAN port [" << port << "] _parameters [" << parameters << "]" << " lib [" << peakLibName << "]";
		}
	} else {
		LOG( Log::TRC ) << __FUNCTION__ << " cca " << _cca->getBusName() << " is created already";
	}
	LOG( Log::DBG ) << "CAN access bus name= " << _cca->getBusName();

	if ( threadData.receiverIsMirror ){
		LOG( Log::DBG ) << __FUNCTION__<< " CAN mirror port= " << threadData.connection_mirror.canPport
				<< " usbport= " << threadData.connection_mirror.usbport;
		threadData.connection_mirror.socketName = getSystecSocketName( threadData.connection_mirror.usbport, threadData.connection_mirror.canPport );
		_ccaMirror = _libloader->openCanBus( threadData.connection_mirror.socketName, parameters);
		LOG( Log::DBG ) << __FUNCTION__<< " CAN mirror access bus name= " << _ccaMirror->getBusName();
	}
	CanxThreads::addOpenPort( port );
}

void CONNECTION::_initCANAnagate( string port, string parameters ){
	string anagateLibName;
#ifdef _WIN32
	anagateLibName = "an";
#else
	anagateLibName = "an";
#endif
	_libloader = CanLibLoader::createInstance( anagateLibName );
	_cca = _libloader->openCanBus( port, parameters );
	if ( _cca != 0 ) {
		LOG( Log::TRC ) << __FUNCTION__ << " SENDER calling createBus";
		bool ret = _cca->createBus( port.c_str(), parameters.c_str() );
		if ( ret ) {
			LOG( Log::TRC ) << __FUNCTION__ << " SENDER open CAN port [" << port << "] _parameters [" << parameters << "]" << " lib [" << anagateLibName << "]";
		} else {
			LOG( Log::WRN ) << __FUNCTION__ << " SENDER CAN port [" << port << "] is already open";
		}
	} else {
		LOG( Log::TRC ) << __FUNCTION__ << " cca " << _cca->getBusName() << " is created already";
	}
	LOG( Log::DBG ) << __FUNCTION__ << " CAN access bus name= " << _cca->getBusName();

	if ( threadData.receiverIsMirror ){
		LOG( Log::DBG ) << __FUNCTION__ << " CAN mirror port= " << threadData.connection_mirror.canPport
				<< " usbport= " << threadData.connection_mirror.usbport;
		threadData.connection_mirror.socketName = "an:0:128.141.159.194"; // getAnagateIpNumber();

		LOG( Log::ERR ) << "todo threadData.connection_mirror.socketName= " << threadData.connection_mirror.socketName;

		_ccaMirror = _libloader->openCanBus( threadData.connection_mirror.socketName, parameters);
		LOG( Log::DBG ) << __FUNCTION__ << " CAN mirror access bus name= " << _ccaMirror->getBusName();
	}
	CanxThreads::addOpenPort( port );
}

void CONNECTION::_deinitCAN( string port ){
#ifdef _WIN32
#else
	LOG( Log::INF  ) << "closing CAN port " << port;
	_libloader->closeCanBus( _cca );
#endif
}


void CONNECTION::executeStartTick( void ){
	LOG( Log::TRC  ) << " executeStartTick ";
	_clock = -1;
}


void CONNECTION::resetSequence( void ){
	LOG( Log::TRC ) << threadData.threadID << " sequence sent "
			<< _messageCounterSequence << " messages";
	_messageCounterSequence = 0;
	_thread_frames_index = 0;
}


/**
 * returns the next frame to play, according to central clock. Any
 * subtick frames are played in the order they are declared. We assume
 * here that the frames are ordered in a monotonous way, from early to late, over all ticks.
 * There can be ticks without frames, ticks with one or many frames.
 */
CanxThreads::CanThreadFrame_t CONNECTION::_nextSequenceFrame( void ){
	CanThreadFrame_t frame;
	for ( unsigned int i = _thread_frames_index; i < _thread_frames.size(); i++ ){
		if ( _thread_frames[ i ].tick == _clock ){
			_thread_frames_index++; // eliminate already played frames from search
			// cout << __FILE__ << " " << __LINE__ << " next _thread_frames_index= " << _thread_frames_index << endl;
			return( _thread_frames[ i ] );
		}
	}
	LOG(Log::WRN) << "next frame in sequence not found, returning dummy frame. Please check your config for 'asConfigured' frames";
	CanThreadFrame_t dummy;
	dummy.tick = -99;
	dummy.canFrame = "dummyframe";
	dummy.id = 0;
	dummy.tick = 0;
	dummy.wait_us = 0;
	return ( dummy );
}

/**
 * comes up with a generated frame with random data
 * Length fixed 8 byte, id fixed, data random
 */
CanxThreads::CanThreadFrame_t CONNECTION::generateRandomFrame( void ){
	CanThreadFrame_t frame;
	int maxID = 2048;
	switch( threadData.canIdType ) {
	case CanIDType_t::CANID_STANDARD:{ maxID = 2048; break; }
	case CanIDType_t::CANID_EXTENDED:{ maxID = 536870911;break;} // 2^29 - 1
	}
	frame.id = rand() % maxID;
	frame.tick = 0;    // unused
	frame.wait_us = threadData.sender_delay_us;
	frame.canFrame = string("\"001.0.0.0.8.") + _gen_random_string( 16 ) + string( ".0000.1\"" );
	return( frame );
}

/** counts ID and data up, when limit reached, starts from 0 again.
 */
CanxThreads::CanThreadFrame_t CONNECTION::generateCountingFrame( void ){
	CanThreadFrame_t frame;
	int maxID = 2048;
	switch( threadData.canIdType ) {
	case CanIDType_t::CANID_STANDARD:{
		maxID = 2047; // 2^11 - 1
		break;
	}
	case CanIDType_t::CANID_EXTENDED:{
		maxID = 536870911; // 2^29 - 1
		break;
	}
	}
	ostringstream convert0;
	convert0 << setfill('0') << setw(3) << hex << _frameCount % maxID;
	string id = convert0.str();
	frame.tick = 0; // unused
	frame.wait_us = threadData.sender_delay_us;

	ostringstream convert1;
	convert1 << setfill('0') << setw(16) << hex << _frameCount; // 16chars of hex data
	string data = convert1.str();

	// LOG(Log::TRC) << "SENDER::generateCountingFrame data= " << data;

	frame.canFrame = string("\"") + id + string(".y.y.y.8.") + data + string( ".yyyy.y\"" );
	_frameCount++;
	// LOG(Log::TRC) << "generateCountingFrame frame.canFrame= " << frame.canFrame;
	return( frame );
}

CanxThreads::CanThreadFrame_t CONNECTION::generateFixedFrame( void ){
	CanThreadFrame_t frame;
	frame.id = 1;
	frame.tick = 0; // unused, any
	frame.wait_us = threadData.sender_delay_us;
	frame.canFrame = "\"001.0.0.0.8.ff00ff11ff22ff33.0000.1\"";
	return( frame );
}

/**
 * generates a random string of determined length from a list of characters
 */
string CONNECTION::_gen_random_string( const unsigned int len ) {
	static const char alphanum[] = "0123456789ABCDEF";
	const int sz = sizeof( alphanum );
	string Result;
	ostringstream convert;
	for ( unsigned int i = 0; i < len; ++i) {
		int index = rand() % ( sz - 1 );
		convert << alphanum[ index ];
	}
	Result = convert.str();
	return( Result );
}

/**
 *  returns the nb of frames to be played in that _clock: 0... many
 *  the frames were configured into a vector
 */
int CONNECTION::_nbFramesPerTick( void ){
	int count = 0;
	for ( unsigned int i = 0; i < _thread_frames.size(); i++ ){
		if ( _thread_frames[ i ].tick == _clock )
			count++;
	}
	// LOG( Log::TRC ) << " SENDER::_nbFramesPerTick clock= " << _clock << " has " << count << " frames (of " << _thread_frames.size() << " total)";
	return( count );
}


/**
 * execute a clock tick as an instance method.
 * shout out if a clock tick was missing, if we would be too late
 * when finally arrived here. This happens when the sequence is
 * faster than what the hardware/software can actually do.
 * Actually then the first constraint is violated:
 * the thread execution must always fit into one tick.
 */
bool CONNECTION::executeClockTick( unsigned int c ){
	LOG( Log::TRC ) << __FUNCTION__ << " id= " << threadData.threadID << " clock tick= " << c;
	_previousClock = _clock;
	_clock = c;
	_messageCounterClockTick = 0;
	LOG( Log::TRC  ) << " executeClockTick " << threadData.threadID
			<< " _clock= " << _clock
			<< " _previousClock= " << _previousClock;

	if ( _previousClock + 1 < _clock ){
		LOG( Log::WRN  ) << " executeClockTick seems we missed a tick: _clock= " << _clock
				<< " _previousClock= " << _previousClock
				<< " id= " << threadData.threadID
				<< " tid= " << threadData.tid
				<< " missed " << _clock - _previousClock - 1 << " ticks";
	}
	if ( _previousClock > _clock ){
		LOG( Log::DBG  ) << " executeClockTick sequence repeat detected: _clock= " << _clock
				<< " _previousClock= " << _previousClock
				<< " id= " << threadData.threadID;
	}
	_measureDelayStart();

	LOG( Log::TRC ) << __FUNCTION__ << " id= " << threadData.threadID << " _sender_enabled= " <<  _sender_enabled;
	if ( ! _sender_enabled ) return( true );

	CanMessage cm;

	// send all messages for this clock tick
	bool sequence_running = true;
	CanThreadFrame_t nframe;
	LOG( Log::TRC ) << " executeClockTick threadData.msgGenerationType= " << threadData.msgGenerationType;

	switch ( threadData.msgGenerationType ){
	case CanxThreads::MSG_UNUSED:{
		_messageCounterSequence++;
		_messageCounterClockTick++;
		break;
	}
	case CanxThreads::MSG_ASCONFIGURED:{
		LOG( Log::INF ) << threadData.threadID << " clock= " << _clock << " nb. frames to send= " << _nbFramesPerTick();
		for ( int i = 0; i < _nbFramesPerTick(); i++ ){
			nframe = _nextSequenceFrame();
			if ( nframe.tick > -1 ){
				if ( !CONNECTION::shutup ) {
					LOG( Log::INF  ) << "SEND ASCONFIGURED FRAME: " << threadData.threadID
							<< " clock= [" << _clock
							<< "] tick= [" << nframe.tick << "]"
							<< "] canFrame= [" << nframe.canFrame << "]";

					cm = _standardCanMsgFromFrame( nframe );
					_cca->sendMessage( &cm );
					CanxThreads::mysleepu( nframe.wait_us );
				} else {
					LOG( Log::TRC ) << threadData.threadID << " clock= " << _clock << " sender is shutup" ;
				}
			}
			_messageCounterSequence++;
			_messageCounterClockTick++;
		}
		break;
	}
	case MSG_COUNTING:{
		LOG( Log::DBG ) << threadData.threadID << " clock= " << _clock << " nb. frames to send= " << threadData.nbMsgToGeneratePerTick;
		for ( unsigned int i = 0; i < threadData.nbMsgToGeneratePerTick; i++ ){
			nframe = generateCountingFrame();
			if ( !CONNECTION::shutup ) {
				LOG( Log::INF  ) << "SEND COUNTING FRAME: " << threadData.threadID
						<< " clock [" << _clock
						<< "] canFrame [" << nframe.canFrame << "]";
				cm = _standardCanMsgFromFrame( nframe );
				_cca->sendMessage( &cm );
				CanxThreads::mysleepu( nframe.wait_us );
			}
			_messageCounterSequence++;
			_messageCounterClockTick++;
		}
		break;
	}
	case MSG_FIXED:{
		LOG( Log::INF ) << threadData.threadID << " clock= " << _clock << " nb. frames to send= " << threadData.nbMsgToGeneratePerTick;
		for ( unsigned int i = 0; i < threadData.nbMsgToGeneratePerTick; i++ ){
			nframe = generateFixedFrame();
			if ( !CONNECTION::shutup ) {
				LOG( Log::INF  ) << "SEND FIXED FRAME: " << threadData.threadID
						<< " clock [" << _clock
						<< "] canFrame [" << nframe.canFrame << "]";

				cm = _standardCanMsgFromFrame( nframe );
				_cca->sendMessage( &cm );
				CanxThreads::mysleepu( nframe.wait_us );
			}
			_messageCounterSequence++;
			_messageCounterClockTick++;
		}
		break;
	}
	case MSG_RANDOM: {
		LOG( Log::INF ) << threadData.threadID << " clock= " << _clock << " nb. frames to send= " << threadData.nbMsgToGeneratePerTick;
		for ( unsigned int i = 0; i < threadData.nbMsgToGeneratePerTick; i++ ){
			nframe = generateRandomFrame();
			if ( !CONNECTION::shutup ) {
				LOG( Log::INF  ) << "SEND RANDOM FRAME: "<< threadData.threadID
						<< " clock [" << _clock
						<< "] canFrame [" << nframe.canFrame << "]";
				cm = _standardCanMsgFromFrame( nframe );
				_cca->sendMessage( &cm );
				CanxThreads::mysleepu( nframe.wait_us );
			}
			_messageCounterSequence++;
			_messageCounterClockTick++;
		}
		break;
	}
	default:{
		LOG( Log::TRC  ) << "executeClockTick: unknown message generation type " << threadData.msgGenerationType;
		return( false ); // some problem, don't do any ticks
	}
	}

	_totalMessagesSent += _messageCounterClockTick;

	/**
	 * when can we end this sender thread ?
	 * - when the sequence (total msg) is finished and there is NO REPEAT
	 * - when the total nb msg to generate is done and there is NO REPEAT
	 * with repeat, the thread just restarts the sequence, and if it is shorter
	 * than another thread, who cares. Thread global sequences will be out of sync with
	 * respect to each other like this.
	 * We can of course convince main to run the same thing several times, that's
	 * a global repeat. Just call it several times....
	 */
	switch( threadData.msgGenerationType ){
	case CanxThreads::MsgGenerationType_t::MSG_ASCONFIGURED:{
		if ( _messageCounterSequence >= _thread_frames.size()
				&& !_repeatSequence )	{
			LOG( Log::TRC  ) << threadData.threadID << " executeClockTick " << _clock << " ending the sequence";
				sequence_running = false;
		}
		break;
	}
	case CanxThreads::MsgGenerationType_t::MSG_COUNTING:
	case CanxThreads::MsgGenerationType_t::MSG_RANDOM:
	case CanxThreads::MsgGenerationType_t::MSG_FIXED:{
		if (( _messageCounterSequence > threadData.nbMsgToGeneratePerTick * _nbGeneratedTicksToExecute )
				&& !_repeatSequence ){
			LOG( Log::TRC  ) << threadData.threadID << " executeClockTick " << _clock
					<< " threadData.nbMsgToGeneratePerTick= " << threadData.nbMsgToGeneratePerTick
					<< " _nbGeneratedTicksToExecute= " << _nbGeneratedTicksToExecute
					<< " ending the sequence";
			sequence_running = false;
		}
		break;
	}
	case CanxThreads::MsgGenerationType_t::MSG_UNUSED:{
		// do nothing for now
		break;
	}
	default: {
		LOG( Log::ERR ) << " executeClockTick: unknown message generation type, check config.";
		exit(0);
		break;
	}
	}
	double delta = _measureDelayStop();
	if ( delta > _globalTickTime ){
		LOG( Log::WRN  ) << " executeClockTick " << _clock
				<< " id= " << threadData.threadID
				<< " clock tick exec took too long:  " << delta << " ms";
	} else {
		LOG( Log::INF  ) << threadData.threadID << " executeClockTick " << _clock
				<< " took " << delta << " ms for "
				<< _messageCounterClockTick << " messages";
	}
	return ( sequence_running );
}

CanMessage CONNECTION::_defaultCanMsg( void ){
	CanMessage cm;
	cm.c_ff = true; // recessive on the CAN bus
	cm.c_time.tv_sec = 0;
	cm.c_time.tv_usec = 0;
	return( cm );
}

/**
 * the frame.data specified is actually the whole can message, including ID, flags etc etc.
 * The frame is a string, with dots as delimiters:
 * ID.RTR.IDE.R0.DLC.DATA.CRC.ACK
 * ID  = 3 chars representing hex range 000...7ff for ID, 11 bit
 * RTR = 1 char representing a boolean RTR, 1 bit
 * IDE = 1 char representing a boolean IDE, 1 bit
 * R0  = 1 char representing a boolean R0, 1 bit
 * DLC = 1 char representing hex range 0...f, 4 bit
 * DATA = 16 chars representing in hex 8 byte, 64bit
 * CRC = 4 chars representing hex range 0000...ffff, 16 bit
 * ACK = 1 char for 1 bit
 * =====
 * makes 28 chars in the string, plus the 7 dots = 35
 *
 * SOF, EOF and IFS not specified
 */
CanMessage CONNECTION::_standardCanMsgFromFrame( CanThreadFrame_t frame ){
	// LOG( Log::TRC ) << " _standardCanMsgFromFrame: [" << frame.canFrame << "]" << " length= " << frame.canFrame.length();
	CanMessage cm;
	string ID  = frame.canFrame.substr(1,3);
	string RTR = frame.canFrame.substr(5,1);
	string IDE = frame.canFrame.substr(7,1);
	string R0  = frame.canFrame.substr(9,1);
	string DLC = frame.canFrame.substr(11,1);
	string DATA = frame.canFrame.substr(13,16);
	string CRC = frame.canFrame.substr(30,4);
	string ACK = frame.canFrame.substr(35,1);

#if 0
	LOG( Log::TRC ) << "ID=" << ID
			<< " RTR="<< RTR
			<< " IDE="<<IDE
			<< " R0="<<R0
			<< " DLC="<<DLC
			<< " DATA="<<DATA
			<< " CRC="<<CRC
			<< " ACK="<<ACK;
#endif
	cm.c_id   = _stringToLongInt( ID );
	cm.c_dlc  = _stringToUChar( DLC );
	cm.c_rtr  = _stringToUChar( RTR );
	cm.c_ff   = _stringToUChar( IDE );
	{
#ifdef WIN32
		GetSystemTime(&_tv);
		cm.c_time.tv_sec = _tv.wSecond;
		cm.c_time.tv_usec = _tv.wMilliseconds;
		//* 1000) + _now.wMilliseconds - (_t1.wSecond * 1000) - _t1.wMilliseconds;
#else
		gettimeofday( &_tv, &_tz );
		cm.c_time.tv_sec = _tv.tv_sec;
		cm.c_time.tv_usec = _tv.tv_usec;
#endif
	}
	if ( cm.c_dlc > 8 ) {
		LOG( Log::WRN ) << " detected a specified CAN frame with more than 8 byte length ["
				<< cm.c_dlc << "] , force it to 8 for now. Check your config file please.";
		cm.c_dlc = 8;
	}
	for ( uint8_t i = 0; i < cm.c_dlc; i++ ){
		cm.c_data[ i ] = _stringToUChar( DATA.substr(2*i,2) ); // per byte = 2chars in hex
	}
	return( cm );
}

/**
 * string hex representation to uchar conversion
 */
uint8_t CONNECTION::_stringToUChar( string s ){
	int ii = 0;
	sscanf( s.c_str(), "%x", &ii );
	return( (uint8_t) ii );
}

/**
 * string hex representation to long int conversion
 */
int64_t CONNECTION::_stringToLongInt( string s ){
	int64_t ii = 0;
	sscanf( s.c_str(), "%lx", &ii );
	return( ii );
}


/**
 * the amount of ticks needed until there is nothing to send any more.
 * This depends also on the way the frames are generated:
 * 	typedef enum { MSG_RANDOM=0, MSG_COUNTING, MSG_FIXED, MSG_SPECIFIED, MSG_UNUSED } MsgGenerationType_t;
 */
int CONNECTION::getTotalTicksSender( void ){
	switch ( threadData.msgGenerationType ){
	case CanxThreads::MSG_ASCONFIGURED:{
		// we can have many frames inside one clock tick, we need just the nb of ticks
		int nbTicks = 0;
		for ( unsigned int i = 0; i < _thread_frames.size(); i++ ){
			if ( _thread_frames[ i ].tick == nbTicks ) nbTicks++;
		}
		return( nbTicks );
		break;
	}
	case MSG_COUNTING:
	case MSG_FIXED:
	case MSG_RANDOM: {
		return( _nbGeneratedTicksToExecute );
	}
	case MSG_UNUSED:
	default:{
		LOG( Log::ERR ) << "maxTicksSender: unknown message generation type " << threadData.msgGenerationType;
		return( 0 ); // some problem, dont do any ticks
	}
	} // switch
	return( 0 );
}

void CONNECTION::addFrame( CanThreadFrame_t f ){
	int sz = _thread_frames.size();
	LOG(Log::INF) << threadData.threadID << " addFrame # " << sz << " to tick " << f.tick << " canFrame= " << f.canFrame;
	_thread_frames.push_back( f );
}



} // namespace sender_ns
