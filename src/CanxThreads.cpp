/*
 * CanxThreads.cpp
 *
 *  Created on: Aug 31, 2017
 *      Author: mludwig
 */

#include <CanLibLoader.h>

#include "CanxThreads.h"

#include "../include/connection.h"


using namespace connection_ns;

namespace canxthreads_ns {


/* static */ int CanxThreads::_instanceCounter = 0;
/* static */ string CanxThreads::_synchronizationMessage = "";

/* static */ CanModule::CanLibLoader *CanxThreads::libloader = NULL;
/* static */ vector<string> CanxThreads::openCanPorts;

/* static */ bool CanxThreads::isCanPortOpen( string port ){
	for ( unsigned int i = 0; i < CanxThreads::openCanPorts.size(); i++ ){
		if ( port == CanxThreads::openCanPorts[ i ] ) {
			LOG( Log::DBG ) << "already open port= " << port;
			return( true );
		}
	}
	LOG( Log::DBG ) << "not yet open port= " << port;
	return( false );
}
/* static */ void CanxThreads::addOpenPort( string port ){
	LOG( Log::DBG ) << "adding port= " << port;
	CanxThreads::openCanPorts.push_back( port );
}

CanxThreads::CanxThreads() {
	_threadLaunched = false;
	_instanceNbCanx = CanxThreads::_instanceCounter;
	_stop = false;

	_clearThreadData();
	threadData.objIndex = _instanceNbCanx;

	CanxThreads::_synchronizationMessage.clear();
	CanxThreads::_instanceCounter++;
}

CanxThreads::~CanxThreads() {
  	LOG(Log::TRC) << "thread: closing " << threadData.objIndex;
}

void CanxThreads::_clearThreadData( void ){
	threadData.sender_enabled = true;
	threadData.receiver_enabled = true;
	threadData.vendor = VENDOR_UNKNOWN;
	threadData.anagate.highSpeed = 0;
	threadData.anagate.operationalMode = 0;
	threadData.anagate.termination = 0;
	threadData.anagate.syncMode = 0;
	threadData.anagate.timeStamp = 0;

	threadData.systec.dummy = 0;
	threadData.threadID = "unknown";
	threadData.objIndex = -1;

	threadData.connection.canLport = -1;
	threadData.connection.canPport = -1;
	threadData.connection.ipNumber = "192.168.1.1";
	threadData.connection.socketName = "unknown";
	threadData.connection.usbport = -1;

	threadData.connection_mirror.canLport = -1;
	threadData.connection_mirror.canPport = -1;
	threadData.connection_mirror.ipNumber = "192.168.1.1";
	threadData.connection_mirror.socketName = "unknown";
	threadData.connection_mirror.usbport = -1;
	threadData.connection_mirror.mirrorIncrementMsgId = false;

	threadData.msgGenerationType = MSG_COUNTING;
	threadData.canIdType = CANID_STANDARD;
	threadData.bitrate = CANBR_125kbs;
	threadData.sbitrate = "CANBR_125kbs";
	threadData.receiver_delay_us = 0;
	threadData.receiverIsRecorder = false;
	threadData.receiverIsMirror = false;
	threadData.nbMsgToGeneratePerTick = 1;

	// for generated messages: counting, fixed, random (NOT sequentially
	// specified) we can slow down. For sequentially specified messages
	// each frame can have it's own delay
	threadData.sender_delay_us = 0;


}

/* static */ void CanxThreads::mysleepu( int us ){
	LOG(Log::TRC) << "CanxThreads::mysleepu wait " << us << " us";
	boost::this_thread::sleep(boost::posix_time::microseconds( us ));
}


/**
 * systec16 has 2 USB ports: usb0[can0..7] and usb1 [can8..15]
 * we specify the usb [0..1] and the can [0..7] port, and have to calculate the logical can port therefore which
 * in this case is from 0..15
 */
string CanxThreads::getSystecSocketName( int usbport /* 0..1 */, int canPport /* physical 0..7 */){
	ostringstream convert;
#ifdef _WIN32
	convert << canPport;
	LOG(Log::TRC) << "getSystecSocketName: for usbport= " << usbport
			<< " canPport= " << canPport << " the systec socket is st:" << convert.str();
	return( "st:" + convert.str() );
#else
	int n = usbport * 8 + canPport;
	convert << n;
	LOG(Log::TRC) << "getSystecSocketName: for usbport= " << usbport
			<< " canPport= " << canPport << " the systec socket is sock:" << convert.str();
	return( "sock:" + convert.str() );
#endif
}

/**
 * PCAN USB-Pro has 2 CAN ports
 */
string CanxThreads::getPeakSocketName( int usbport /* 0..1 */, int canPport /* physical 0..7 */){
	ostringstream convert;
#ifdef _WIN32
	convert << canPport;
	LOG(Log::TRC) << "getSystecSocketName: for usbport= " << usbport
			<< " canPport= " << canPport << " the peak socket is pk:" << convert.str();
	return( "pk:" + convert.str() );
#else
	int n = usbport * 8 + canPport;
	convert << n;
	LOG(Log::TRC) << "getSystecSocketName: for usbport= " << usbport
			<< " canPport= " << canPport << " the peak socket is ??? sock:" << convert.str();
	return( "sock:" + convert.str() );
#endif
}


string CanxThreads::codeBR_2_string( CanBitrates_t br ){
	switch ( br ){
	case CANBR_1000kbs: return("1000000");
	case CANBR_800kbs: return("800000");
	case CANBR_500kbs: return("500000");
	case CANBR_250kbs: return("250000");
	case CANBR_125kbs: return("125000");
	case CANBR_50kbs: return("50000");
	case CANBR_20kbs: return("20000");
	case CANBR_10kbs: return("10000");
	default: {
		LOG(Log::WRN) << " codeBR_2_string: unknown bitrate " << br << " found in configuration, assuming default 125000";
		return("125000");
	}
	}
}

canxthreads_ns::CanxThreads::CanBitrates_t CanxThreads::codeString_2_BR( string sbr ){
	if ( sbr == "1000000") return( CANBR_1000kbs );
	if ( sbr == "800000") return( CANBR_800kbs );
	if ( sbr == "500000") return( CANBR_500kbs );
	if ( sbr == "250000") return( CANBR_250kbs );
	if ( sbr == "125000") return( CANBR_125kbs );
	if ( sbr == "50000") return( CANBR_50kbs );
	if ( sbr == "20000") return( CANBR_20kbs );
	if ( sbr == "10000") return( CANBR_10kbs );
	LOG(Log::WRN) << " codeString_2_BR: unknown sbitrate " << sbr << " found in configuration, assuming default 0 (125000)";
	return( CANBR_125kbs );
}

#if 0
/**
 * logical port
 */
int CanxThreads::getSystecCanLport( ThreadData_t td  ){
#ifdef _WIN32
	return( td.connection.canPport );
#else
	// systec: convert physical can port (hardware) into logical can port (driver)
	int ncan = 0;
	if ( td.connection.usbport == 1 ){ ncan = 8; }
	return( td.connection.canPport - ncan );
#endif
}
#endif

/**
 * anagate ip number (instead of usb port)
 */
std::string CanxThreads::getAnagateIpNumber( ThreadData_t td  ){
	return( td.connection.ipNumber );
}

/**
 * for multiport modules A, B, ... we number them 0, 1, ...
 * even if they are strings
 * this is the physical port number on the module, NOT the
 * logical port number used by the driver
 */
std::string CanxThreads::getAnagatePport( ThreadData_t td  ){
	return( std::to_string( td.connection.canPport ));
}


// block until a message of data content wm has been received
void CanxThreads::waitForMessage( string wm ){
	LOG(Log::INF) << "_waitForMessage block until " << wm << " is received";
	while ( wm != CanxThreads::_synchronizationMessage ){
		mysleepu( 100 );
	}
	LOG(Log::INF) << "_waitForMessage: received " << wm << " , continue";
}

bool CanxThreads::detectMessage( string wm ){
	LOG(Log::INF) << "_detectMessage " << wm;
	if ( wm == CanxThreads::_synchronizationMessage ) return( true );
	else return( false );
}
} /* namespace canxthreads_ns */
