CANL linux/linux transmission tests


slc6:
[ml@pcitco04 CAN]$ export LD_LIBRARY_PATH=/opt/boost/boost_1_59_0/lib
[ml@pcitco04 CAN]$ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./libsocketcan/
[ml@pcitco04 CAN]$ ldd ./CANL-systec-test-0.1.bin 
	linux-vdso.so.1 =>  (0x00007fff60394000)
	libboost_log.so.1.59.0 => /opt/boost/boost_1_59_0/lib/libboost_log.so.1.59.0 (0x00007f0f48fe2000)
	libboost_log_setup.so.1.59.0 => /opt/boost/boost_1_59_0/lib/libboost_log_setup.so.1.59.0 (0x00007f0f48d3b000)
	libboost_filesystem.so.1.59.0 => /opt/boost/boost_1_59_0/lib/libboost_filesystem.so.1.59.0 (0x00007f0f48b25000)
	libboost_program_options.so.1.59.0 => /opt/boost/boost_1_59_0/lib/libboost_program_options.so.1.59.0 (0x00007f0f488bd000)
	libboost_system.so.1.59.0 => /opt/boost/boost_1_59_0/lib/libboost_system.so.1.59.0 (0x00007f0f486b9000)
	libboost_chrono.so.1.59.0 => /opt/boost/boost_1_59_0/lib/libboost_chrono.so.1.59.0 (0x00007f0f484b2000)
	libboost_date_time.so.1.59.0 => /opt/boost/boost_1_59_0/lib/libboost_date_time.so.1.59.0 (0x00007f0f4829e000)
	libboost_thread.so.1.59.0 => /opt/boost/boost_1_59_0/lib/libboost_thread.so.1.59.0 (0x00007f0f4807b000)
	libsocketcan.so.2 => ./libsocketcan/libsocketcan.so.2 (0x00007f0f47e78000)
	libdl.so.2 => /lib64/libdl.so.2 (0x0000003e99800000)
	libstdc++.so.6 => /usr/lib64/libstdc++.so.6 (0x0000003ea4000000)
	libm.so.6 => /lib64/libm.so.6 (0x0000003e9a000000)
	libc.so.6 => /lib64/libc.so.6 (0x0000003e99c00000)
	libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x0000003ea3800000)
	libpthread.so.0 => /lib64/libpthread.so.0 (0x0000003e9a400000)
	libboost_regex.so.1.59.0 => /opt/boost/boost_1_59_0/lib/libboost_regex.so.1.59.0 (0x00007f0f47b6d000)
	librt.so.1 => /lib64/librt.so.1 (0x0000003e9a800000)
	/lib64/ld-linux-x86-64.so.2 (0x0000003e99400000)


cc7:
	[root@pcbe13632 CANX-systec-test]# ldd CANX-systec-test
	linux-vdso.so.1 =>  (0x00007ffe128ad000)
	libboost_log.so.1.59.0 => /opt/boost/vanilla_1_59_0/stage/lib/libboost_log.so.1.59.0 (0x00007fbf202c6000)
	libboost_log_setup.so.1.59.0 => /opt/boost/vanilla_1_59_0/stage/lib/libboost_log_setup.so.1.59.0 (0x00007fbf20023000)
	libboost_filesystem.so.1.59.0 => /opt/boost/vanilla_1_59_0/stage/lib/libboost_filesystem.so.1.59.0 (0x00007fbf1fe0c000)
	libboost_program_options.so.1.59.0 => /opt/boost/vanilla_1_59_0/stage/lib/libboost_program_options.so.1.59.0 (0x00007fbf1fb9a000)
	libboost_system.so.1.59.0 => /opt/boost/vanilla_1_59_0/stage/lib/libboost_system.so.1.59.0 (0x00007fbf1f995000)
	libboost_chrono.so.1.59.0 => /opt/boost/vanilla_1_59_0/stage/lib/libboost_chrono.so.1.59.0 (0x00007fbf1f78e000)
	libboost_date_time.so.1.59.0 => /opt/boost/vanilla_1_59_0/stage/lib/libboost_date_time.so.1.59.0 (0x00007fbf1f57c000)
	libboost_thread.so.1.59.0 => /opt/boost/vanilla_1_59_0/stage/lib/libboost_thread.so.1.59.0 (0x00007fbf1f35b000)
	libsocketcan.so.2 => /home/mludwig/CANCommonComponentWorkspace/CANX-systec-test/../libsocketcan/src/.libs/libsocketcan.so.2 (0x00007fbf1f157000)
	libdl.so.2 => /lib64/libdl.so.2 (0x00007fbf1ef21000)
	libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007fbf1ec17000)
	libm.so.6 => /lib64/libm.so.6 (0x00007fbf1e915000)
	libc.so.6 => /lib64/libc.so.6 (0x00007fbf1e554000)
	libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007fbf1e33d000)
	libboost_regex.so.1.59.0 => /opt/boost/vanilla_1_59_0/stage/lib/libboost_regex.so.1.59.0 (0x00007fbf1e02f000)
	libicudata.so.50 => /lib64/libicudata.so.50 (0x00007fbf1ca5b000)
	libicui18n.so.50 => /lib64/libicui18n.so.50 (0x00007fbf1c65b000)
	libicuuc.so.50 => /lib64/libicuuc.so.50 (0x00007fbf1c2e2000)
	librt.so.1 => /lib64/librt.so.1 (0x00007fbf1c0da000)
	libpthread.so.0 => /lib64/libpthread.so.0 (0x00007fbf1bebd000)
	/lib64/ld-linux-x86-64.so.2 (0x00007fbf20569000)
	