#!/bin/csh
find ./ -name "CMakeCache.txt" -exec rm {} \;
find ./ -name "CMakeFiles" -exec rm -rf {} \;
rm -rf CMakeFiles
rm -rf html
rm -rf latex
# eliminate 3rd party dependencies on code level (git clones) 
#rm -rf ./LogIt
#rm -rf ./CanModule


# come up with a message for git
set MSG1=`date`" quick push from eclipse from "`whoami`"@"`hostname`
echo ${MSG1}
rm -f ./tmp_msg.txt
echo ${MSG1}"\n" > ./tmp_msg.txt
gedit ./tmp_msg.txt
set MSG=`cat ./tmp_msg.txt`

git add --all
git commit -m "${MSG}"
git push origin master


