#!/bin/csh
# package all binary libs and needed headers for CanModule into an archive
# excluding for now the dynamically loaded vendor libs


set VERSION="0.9.9"
set BINTGZ="CanModule-${VERSION}.tar.gz"
set BINLIBS="CanModule/lib/libCanModule.so CanInterfaceImplementations/output/libancan.so CanInterfaceImplementations/output/libMockUpCanImplementationcan.so CanInterfaceImplementations/output/libsockcan.so"   
set HEADERS="CanModule/CanInterface/include/CanBusAccess.h CanModule/CanInterface/include/CanMessage.h CanModule/CanInterface/include/CanStatistics.h CanModule/CanInterface/include/CCanAccess.h CanModule/CanInterface/include/CanBusAccess.h CanModule/CanInterface/include/ExportDefinition.h CanModule/CanLibLoader/include/CanLibLoader.h CanModule/LogIt/include/ComponentAttributes.h CanModule/LogIt/include/LogIt.h CanModule/LogIt/include/LogItInstance.h CanModule/LogIt/include/LogItStaticDefinitions.h CanModule/LogIt/include/LogLevels.h CanModule/LogIt/include/LogRecord.h CanModule/LogIt/include/LogSinkInterface.h CanModule/LogIt/include/LogSinks.h CanModule/CWrapper/include/canmodule_wrapper.h"
set SOURCES="CanModule/CWrapper/src/canmodule_wrapper.cpp"
rm -vrf ./CanModule_binary
mkdir -p ./CanModule_binary/libs
mkdir -p ./CanModule_binary/include
mkdir -p ./CanModule_binary/src
echo "version= "${VERSION}" tgz= "${BINTGZ}" bin libs= "${BINLIBS}
foreach f ( ${BINLIBS} )
	cp -v $f ./CanModule_binary/libs
end; 
foreach f ( ${HEADERS} )
	cp -v $f ./CanModule_binary/include
end; 
foreach f ( ${SOURCES} )
	cp -v $f ./CanModule_binary/src
end; 

tar -pczf ${BINTGZ} ./CanModule_binary
rm -rf ./CanModule_binary
echo "CanModule tgz is at "`pwd`/${BINTGZ}

#
# push to nexus
#
set ARCH="cc7"
set NREPO="cern-can"
set REPO="https://repository.cern.ch/nexus/content/repositories/${NREPO}/${ARCH}/CanModuleCombo/${VERSION}"
echo ""
echo "========================================"
echo "nexus repo= "${REPO}
echo "pushing CanModule TGZ to nexus ? (y/n)"
echo "========================================"

set ANSWER = $<
if ( ${ANSWER} == "y") then
	echo "uploading to nexus"
	echo "uploading ${BINTGZ} to ${REPO}/${BINTGZ} (authenticate in separate xterm)"
	xterm -bg orange -e "curl -v -u mludwig --upload-file ./${BINTGZ} ${REPO}/${BINTGZ}; sleep 10; "
else
	echo "doing nothing"
endif


