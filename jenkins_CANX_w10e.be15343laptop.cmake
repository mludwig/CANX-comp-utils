# toolchain for w10e CANX-tester for CI jenkins physical laptop
# mludwig at cern dot ch
# cmake -DCMAKE_TOOLCHAIN_FILE=jenkins_CANX_w10e.be15343laptop.cmake .
#
#
# boost
#
SET ( BOOST_PATH_LIBS "C:/3rdPartySoftware/boost/1.59.0/DEFAULT_NAMESPACE_INSTALL/64bit/lib" )
SET ( BOOST_HEADERS "C:/3rdPartySoftware/boost/1.59.0/DEFAULT_NAMESPACE_INSTALL/64bit/include" )
SET ( BOOST_LIBS 
	libboost_log-vc140-mt-gd-1_59.lib
	libboost_log_setup-vc140-mt-gd-1_59.lib
	libboost_filesystem-vc140-mt-gd-1_59.lib
	libboost_program_options-vc140-mt-gd-1_59.lib 
	libboost_system-vc140-mt-gd-1_59.lib
	libboost_date_time-vc140-mt-gd-1_59.lib 
	libboost_thread-vc140-mt-gd-1_59.lib  )

# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_HEADERS   "$ENV{JENKINSWS}/CanModule/LogIt/include" )
SET ( LOGIT_PATH_LIBS "$ENV{JENKINSWS}/CanModule/LogIt/lib" )
SET ( LOGIT_LIBS "-lLogIt" )

#
# xerces-c, build on physical be15343 laptop
#
SET ( XERCES_PATH_LIBS "$ENV{JENKINSWS}/../xerces.w10e.be15343laptop/src/Debug" )
SET ( XERCES_HEADERS "$ENV{JENKINSWS}/../xerces.w10e.be15343laptop/src" )
SET ( XERCES_LIBS "xerces-c_3D.lib" )


# vendor libs for windows to be added. We inject them
#
# systec
# version 6.02 for windows 10 7may2018
SET( SYSTEC_LIB_FILE "C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Lib/USBCAN64.lib")
SET( SYSTEC_INC_DIR "C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Include")
SET( SYSTEC_PATH_LIBS "C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/lib" )

# anagate
# version vc8 as it seems
SET( ANAGATE_LIB_FILE "C:/3rdPartySoftware/AnaGateCAN/win64/vc8/Release/AnaGateCanDll64.lib")
SET( ANAGATE_INC_DIR "C:/3rdPartySoftware/AnaGateCAN/win64/vc8/include" )
SET( ANAGATE_PATH_LIBS "C:/3rdPartySoftware/AnaGateCAN/win64/vc8/Release" )




