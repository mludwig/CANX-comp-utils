# Change Log CANX
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [in progress]

### to add
- sphinx/breathe documentation
- improve/cleanup comments in the code
- code cleanup
- look at possibilities to show full driver info and module info like
  i.e. firmware versions etc, without going through CanModule in the first place
- integrate peak modules/windows
- systec/socketCan reconnect
### to change
### to fix
- CanModule stats per port

## [1.0.3] - 4-april-2019
### Added
### Changed
- LogIt WRN level for statistics
- recorder: allocate on heap for slightly more performance
### Fixed


## [1.0.2] - 3-april-2019
### Added
- total message counters for send/receive per CAN port     
### Changed
### Fixed

## [1.0.1] - 21-march-2019
### Added
- based on CanModule 1.0.1, with anagate full reconnect
- added msg id increment ( id++ ) for mirror messages into the config of the receiver:
  add the (optional) attribute mirrorIncrementMsgId="true" 
  to the receive config, with mirror=true, to have each mirrored CAN message
  with a msg id incremented.
- added command line option "-anagateSoftwareReset" to perform an anagate firmware
  reset, using anagates API. This is a one-shot action which does not communicate
  to the module via CanModule at all, it just calls the API. Another client using 
  CanModule (i.e. another instance of CANX doing send/receive in parallel, or an OPC-UA server ...)
  should recuperate the anagate connection automatically (CanModule takes care of that).     
### Changed
### Fixed

## [0.0.15] - 14-sept-2018
### Added
- 32/64bit for w2008r2, w2012r2, w2016s (cc7 stays on 64bit)
- support for different vendor modules in config
- using cmake-3.1 or higher (actually 3.11.3) to be compatible with googletest

### Changed
### Fixed


## [0.0.14] - 26-june-2018
### Added

### Changed
- CanxThreads superclass, sync mechanism

### Fixed
- clock tick threading behaviour using boost::barrier
- repeat sequence

## [0.0.13] - 19-june-2018
### Added
- xml configurable
- receiver with mirror and recorder

### Changed
- many small fixes and changes, start to be useable

### Fixed
- can message sending and receiving
- LogIt ok now

## [0.0.12] - 16-may-2018
### Added
- CHANGELOG.md

### Changed

### Fixed

### Removed

