# toolchain for w10e CANX-tester for CI jenkins
# mludwig at cern dot ch
# cmake -DCMAKE_TOOLCHAIN_FILE=jenkins_CANX_w10e.cmake .
#
#
# boost
#
SET ( BOOST_PATH_LIBS "C:/3rdPartySoftware/boost/1.59.0/DEFAULT_NAMESPACE_INSTALL/64bit/lib" )
SET ( BOOST_HEADERS "C:/3rdPartySoftware/boost/1.59.0/DEFAULT_NAMESPACE_INSTALL/64bit/include" )
SET ( BOOST_LIBS 
	libboost_log-vc140-mt-gd-1_59.lib
	libboost_log_setup-vc140-mt-gd-1_59.lib
	libboost_filesystem-vc140-mt-gd-1_59.lib
	libboost_program_options-vc140-mt-gd-1_59.lib 
	libboost_system-vc140-mt-gd-1_59.lib
	libboost_date_time-vc140-mt-gd-1_59.lib 
	libboost_thread-vc140-mt-gd-1_59.lib  )

# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_HEADERS   "$ENV{JENKINSWS}/CanModule/LogIt/include" )
SET ( LOGIT_PATH_LIBS "$ENV{JENKINSWS}/CanModule/LogIt/lib" )
SET ( LOGIT_LIBS "-lLogIt" )

#
# xerces-c
#
SET ( XERCES_PATH_LIBS "$ENV{JENKINSWS}/../xerces.w10e/src/Debug" )
SET ( XERCES_HEADERS "$ENV{JENKINSWS}/../xerces.w10e/src" )
SET ( XERCES_LIBS "xerces-c_3D.lib" )


# vendor libs for windows to be added...
#
# systec: we use socketcan for linux
#
SET(SYSTEC_HEADERS "/home/mludwig/CAN/CAN_libsocketcan/include")
SET(SYSTEC_PATH_LIBS "/home/mludwig/CAN/CAN_libsocketcan/src/.libs")
SET(SYSTEC_LIB_FILE "-lsocketcan")
include_directories ( ${SYSTEC_INC_DIR} )
#
# anagate: we use a TCP convenience library from anagate
#
SET ( ANAGATE_PATH_LIBS "/home/mludwig/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6" )
SET ( ANAGATE_INC_DIR "/home/mludwig/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6/include" )
SET ( ANAGATE_LIB_FILE "-lAPIRelease64 -lCANDLLRelease64" )

# peak
# version PCAN Basic 4.3.2
SET( PEAKCAN_LIB_FILE "PCANBasic.lib")
SET( PEAKCAN_HEADERS "M:/3rdPartySoftware/PCAN-Basic API/Include" )
SET( PEAKCAN_PATH_LIBS "M:/3rdPartySoftware/PCAN-Basic API/x64/VC_LIB" )
