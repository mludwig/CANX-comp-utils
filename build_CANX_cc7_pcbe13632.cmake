# toolchain for cc7 CANX
# cmake -DCMAKE_TOOLCHAIN_FILE= <toolchainname>.cmake .
#

message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CC7 build for CANX-tester" )

#
# boost
#
SET ( BOOST_PATH_LIBS "/home/mludwig/3rdPartySoftware/boost/vanilla_1_59_0/stage/lib" )
SET ( BOOST_HEADERS   "/home/mludwig/3rdPartySoftware/boost/vanilla_1_59_0" )
SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_program_options 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )

# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_HEADERS   "CanModule/LogIt/include" )
SET ( LOGIT_PATH_LIBS "CanModule/LogIt/lib" )
SET ( LOGIT_LIBS "-lLogIt" )

#
# xerces-c
#
SET ( XERCES_PATH_LIBS "../xerces-c/src/Debug" )
SET ( XERCES_HEADERS "../xerces-c/src" )
SET ( XERCES_LIBS "-lxerces-c" )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_HEADERS= ${XERCES_HEADERS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_PATH_LIBS= ${XERCES_PATH_LIBS} ")

# we build CanModule from the sources, and we need headers and libs from the vendors
#
# systec: we use socketcan for linux
#
SET(SYSTEC_HEADERS "/home/mludwig/CAN/CAN_libsocketcan/include")
SET(SYSTEC_PATH_LIBS "/home/mludwig/CAN/CAN_libsocketcan/src/.libs")
SET(SYSTEC_LIB_FILE "-lsocketcan")
include_directories ( ${SYSTEC_HEADERS} )
#
# anagate: we use a TCP convenience library from anagate
#
SET ( ANAGATE_PATH_LIBS "/home/mludwig/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6" )
SET ( ANAGATE_HEADERS "/home/mludwig/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6/include" )
SET ( ANAGATE_LIB_FILE "-lAPIRelease64 -lCANDLLRelease64" )
message( STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ANAGATE_PATH_LIBS= ${ANAGATE_PATH_LIBS} " )
message( STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ANAGATE_INC_DIR= ${ANAGATE_INC_DIR} " )
message( STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ANAGATE_LIB_FILE= ${ANAGATE_LIB_FILE} " )
include_directories ( ${ANAGATE_HEADERS} )
#
# peak: PCANBasic vendor supplied open source for linux
#
SET(PEAKCAN_HEADERS "/home/mludwig/3rdPartySoftware/Peak/PCAN_Basic_Linux-4.2.2/pcanbasic")
SET(PEAKCAN_PATH_LIBS "/home/mludwig/3rdPartySoftware/Peak/PCAN_Basic_Linux-4.2.2/pcanbasic")
SET(PEAKCAN_LIB_FILE "-lpcanbasic")
include_directories ( ${PEAKCAN_HEADERS} )
#
# special functions not using CanModule
#
SET ( SPECIAL_PATH_LIBS /home/mludwig/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6
	/home/mludwig/3rdPartySoftware/Anagate/CAN/libAnaGateExt-1.0.3/linux64/gcc4_6
	/home/mludwig/3rdPartySoftware/Anagate/CAN/libAnaGate-1.0.9/linux64/gcc4_6 )
SET ( SPECIAL_HEADERS ${ANAGATE_INC_DIR} )	
SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} -lAnaGateExtRelease )


