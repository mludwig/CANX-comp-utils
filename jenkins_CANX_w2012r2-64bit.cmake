# toolchain for w2012r2 CANX-tester for CI jenkins 64bit
# mludwig at cern dot ch
# cmake -DCMAKE_TOOLCHAIN_FILE=jenkins_CANX_w2012r2.cmake .
option(64BIT "64 bit build" ON) 
#
#
# boost
#	
# bin download from sl	
SET ( BOOST_PATH_LIBS "M:/3rdPartySoftware/boost_1_59_0-msvc-14/lib64" )
SET ( BOOST_HEADERS "M:/3rdPartySoftware/boost_1_59_0-msvc-14" )
SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )
	
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_PATH_LIBS:${BOOST_PATH_LIBS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_HEADERS:${BOOST_HEADERS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain defines [BOOST_LIBS:${BOOST_LIBS}]" )

# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_HEADERS   "$ENV{JENKINSWS}/CanModule/LogIt/include" )
SET ( LOGIT_PATH_LIBS "$ENV{JENKINSWS}/CanModule/LogIt/lib" )
SET ( LOGIT_LIBS "-lLogIt" )

#
# xerces-c
#
SET ( XERCES_PATH_LIBS "M:/3rdPartySoftware/xerces-c-3.2.0_64bit/src/Debug" )
SET ( XERCES_HEADERS "M:/3rdPartySoftware/xerces-c-3.2.0_64bit/src" )
SET ( XERCES_LIBS "xerces-c_3D.lib" )


# vendor libs for windows to be added...
#
# systec
# version 6.02 for windows 10 7may2018
SET( SYSTEC_LIB_FILE "M:/3rdPartySoftware/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Lib/USBCAN64.lib")
SET( SYSTEC_HEADERS "M:/3rdPartySoftware/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Include")
SET( SYSTEC_PATH_LIBS "M:/3rdPartySoftware/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/lib" )

# anagate
# version vc8 as it seems, w32 only, static lib
SET( ANAGATE_LIB_FILE "M:/3rdPartySoftware/AnaGateCAN/win64/vc8/Release/AnaGateCanDll64.lib")
SET( ANAGATE_HEADERS "M:/3rdPartySoftware/AnaGateCAN/win64/vc8/include" )
SET( ANAGATE_PATH_LIBS "M:/3rdPartySoftware/AnaGateCAN/win64/vc8/Release" )




