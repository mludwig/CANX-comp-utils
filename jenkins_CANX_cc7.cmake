# toolchain for cc7 CANX
# mludwig at cern dot ch
# cmake -DCMAKE_TOOLCHAIN_FILE=jenkins_CANX_cc7.cmake .
#
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CC7 build for CANX-tester")
#
# boost: we downloaded a readymade cc7 install from nexus, to save compile time
#set( BOOST_LIBS ${libboostlogsetup} ${libboostlog} ${libboostsystem} ${libboostfilesystem} ${libboostthread} ${libboostprogramoptions} ${libboostchrono} ${libboostdatetime} -lrt)

SET ( BOOST_PATH_LIBS "$ENV{JENKINSWS}/boost/lib" )
SET ( BOOST_HEADERS   "$ENV{JENKINSWS}/boost/include" )
SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_program_options 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_PATH_LIBS= ${BOOST_PATH_LIBS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_LIBS= ${BOOST_LIBS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_HEADERS= ${BOOST_HEADERS} ")

# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_HEADERS   "$ENV{JENKINSWS}/CanModule/LogIt/include" )
SET ( LOGIT_PATH_LIBS "$ENV{JENKINSWS}/CanModule/LogIt/lib" )
SET ( LOGIT_LIBS "-lLogIt" )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LOGIT_HEADERS= ${LOGIT_HEADERS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LOGIT_PATH_LIBS= ${LOGIT_PATH_LIBS} ")

#
# xerces-c: must be installed on local machine
#
SET ( XERCES_PATH_LIBS "/usr/local/lib" )
SET ( XERCES_HEADERS "/usr/local/include" )
SET ( XERCES_LIBS "libxerces-c-3.1.so" )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_HEADERS= ${XERCES_HEADERS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_PATH_LIBS= ${XERCES_PATH_LIBS} ")
#
# systec: we use socketcan for linux
#
SET(SYSTEC_HEADERS "/usr/local/include")
SET(SYSTEC_PATH_LIBS "/usr/local/lib")
SET(SYSTEC_LIB_FILE "-lsocketcan")
#include_directories ( ${SYSTEC_HEADERS} )
#
# anagate: we use a TCP convenience library from anagate
#
SET ( ANAGATE_PATH_LIBS "/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6" )
SET ( ANAGATE_INC_DIR "/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6/include" )
SET ( ANAGATE_LIB_FILE "-lAPIRelease64 -lCANDLLRelease64" )
message( STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ANAGATE_PATH_LIBS= ${ANAGATE_PATH_LIBS} " )
message( STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ANAGATE_INC_DIR= ${ANAGATE_INC_DIR} " )
message( STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ANAGATE_LIB_FILE= ${ANAGATE_LIB_FILE} " )
#
# peak: socketcan for linux
#
#SET(PEAKCAN_HEADERS "/usr/local/include")
#SET(PEAKCAN_PATH_LIBS "/usr/local/lib")
#SET(PEAKCAN_LIB_FILE "-lsocketcan")
#include_directories ( ${PEAKCAN_HEADERS} )
#
# peak: PCANBasic vendor supplied open source for linux
#
SET(PEAKCAN_HEADERS "/opt/3rdPartySoftware/Peak/PCAN_Basic_Linux-4.2.2/pcanbasic"
	"/opt/3rdPartySoftware/Peak/PCAN_Basic_Linux-4.2.2/pcanbasic/src/libpcanfd" )
SET(PEAKCAN_PATH_LIBS "/opt/3rdPartySoftware/Peak/PCAN_Basic_Linux-4.2.2/pcanbasic")
SET(PEAKCAN_LIB_FILE "-lpcanbasic")
include_directories ( ${PEAKCAN_HEADERS} )
#
# special functions not using CanModule
#
SET ( SPECIAL_PATH_LIBS 
	/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6
	/opt/3rdPartySoftware/Anagate/CAN/libAnaGateExt-1.0.3/linux64/gcc4_6
	/opt/3rdPartySoftware/Anagate/CAN/libAnaGate-1.0.9/linux64/gcc4_6 
)
SET ( SPECIAL_HEADERS ${ANAGATE_INC_DIR} 
	/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6/include
)	
SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} -lAnaGateExtRelease )

