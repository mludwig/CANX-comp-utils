/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: Wed Aug 30 10:15:57 CEST 2017
 *
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *      Contributors and prior art: Benjamin Farnham, Piotr Nikiel, Viacheslav Filimonov
 *
 * This file is part of the CAN Common Tools project and is the property of CERN, Geneva, Switzerland,
 * and is not free software, since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */
/** main.h
 *
 */

#ifndef CANX_COMP_UTILS_INCLUDE_MAIN_H
#define CANX_COMP_UTILS_INCLUDE_MAIN_H

#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/signals2/signal.hpp>
#include <boost/foreach.hpp>
#include <boost/atomic.hpp>

#include <utility>


#endif
