/*
 * Configuration.h
 *
 *  Created on: May 7, 2018
 *      Author: mludwig
 */

#ifndef SRC_CONFIGURATION_H_
#define SRC_CONFIGURATION_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <map>
#include <vector>

#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include "CanxThreads.h"

namespace configuration_ns {

#define NODE_NAME_MAX 127
#define FRAME_STANDARD_LENGTH 35 //configured frames

class Configuration {
public:
	Configuration();
	virtual ~Configuration();
	void read( std::string filename );

private:
	std::string _configfile;
	xercesc::XercesDOMParser *parser;
	inline bool _fileExists( const std::string& name );

	// xerces xml internals
	XMLCh* _TAG_root;;
	XMLCh* _TAG_global;
	XMLCh* _TAG_thread;
	XMLCh* _TAG_sender;
	XMLCh* _TAG_receiver;
	XMLCh* _TAG_tick;
	XMLCh* _TAG_frame;
	XMLCh* _GLOBAL_ATTR_ticksNumber;
	XMLCh* _GLOBAL_ATTR_tickTime;
	XMLCh* _GLOBAL_ATTR_repeat;
	XMLCh* _THREAD_ATTR_port;
	XMLCh* _THREAD_ATTR_speed;
	XMLCh* _SENDER_ATTR_name;
	XMLCh* _RECEIVER_ATTR_name;
	XMLCh* _RECEIVER_ATTR_mirror;
	XMLCh* _RECEIVER_ATTR_mport;
	XMLCh* _TICK_ATTR_index;
	XMLCh* _FRAME_value;

public:
	// config structures
	typedef struct {
		int nbTicks;
		int tickTime; // ms
		int repeat;
	} CFG_GLOBAL_t;
	typedef struct {
		int index;
		std::vector<std::string> frame_v;
	} CFG_TICK_t;
	typedef struct {
		int generatedNbFramesPerTick;   // only used if frames are counted or random
		int waitBetweenFrames; // us, approx
		bool enabled;
		canxthreads_ns::CanxThreads::MsgGenerationType_t messageGeneration;
		canxthreads_ns::CanxThreads::CanIDType_t messageStandard;
		std::vector<CFG_TICK_t> tick_v;
	} CFG_SENDER_t;
	typedef struct {
		bool enabled;
		bool mirror;
		int mirrorUsbPort;
		int mirrorCanPort;
		bool mirrorIncrementMsgId;
		bool recorder;
		int waitBetweenFrames;
		canxthreads_ns::CanxThreads::CanIDType_t messageStandard;
	} CFG_RECEIVER_t;
	typedef struct {
		// all boards
		int canPort;
		string moduleType;
		canxthreads_ns::CanxThreads::CanBitrates_t bitrate;
		CFG_SENDER_t sender;
		CFG_RECEIVER_t receiver;
		int threadIndex;

		// systec
		int usbPort;

		// anagate
		string ipNumber;
		int operationMode;
		int termination;
		int highSpeed;
		int timeStamp;
		int syncMode;
	} CFG_THREAD_t;

private:
	CFG_GLOBAL_t  _global;
	std::vector<CFG_THREAD_t>  _thread_v;
	std::string _getAttributeValue( xercesc::DOMNamedNodeMap *attributes, std::string attr_name );

public:
	void setLogLevel( Log::LOG_LEVEL ll ){		Log::setNonComponentLogLevel( ll ); };

	// config access read, copy
	CFG_GLOBAL_t global( void ){ return ( _global ); }
	unsigned int nbThreads( void ){ return _thread_v.size(); }
	CFG_THREAD_t thread( int ithread ){ return ( _thread_v[ ithread ] ); }
	int nbFrames( int ithread ){ return ( _thread_v[ ithread ].sender.tick_v.size() ); }
};

} /* namespace ns_configuration */

#endif /* SRC_CONFIGURATION_H_ */
