/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: Wed Aug 30 10:15:57 CEST 2017
 *
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *      Contributors and prior art: Benjamin Farnham, Piotr Nikiel, Viacheslav Filimonov
 *
 * This file is part of the CAN Common Tools project and is the property of CERN, Geneva, Switzerland,
 * and is not free software, since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */
/*
 * sender.h
 *
 *  Created on: Aug 11, 2017
 *      Author: mludwig
 */

#ifndef SENDER_H_
#define SENDER_H_
#include <iomanip>
#include <fstream>

#include <boost/lexical_cast.hpp>
#include <boost/thread/thread.hpp>
#include <CanLibLoader.h>
#include <CanBusAccess.h>
#include <CCanAccess.h>

#include <LogIt.h>
#ifndef _WIN32
#include <unistd.h>
#endif

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif


#include "CanxThreads.h"

using namespace canxthreads_ns;
using namespace std;
using namespace CanModule;

using boost::this_thread::get_id;

namespace connection_ns {


#define MAX_CAN_MSG 4095 // unsigned char max length, ISO 15765-2:2016
#define MAX_FRAME_COUNT 9999999

/**
 * Send CAN frames to CAN ports, in a multi-threaded but strictly clocked environment.
 * One can port == connection object == two threads, one for sending, one for receiving in a handler.
 * Each sender(-object) is initialized
 * and reads in a sequence of {clock-tick, sequence-number, frame}. This determines
 * precisely what the thread will send to its' associated CAN port, and when, and in
 * what order. Also the frame-data and frame-is should be specified (unless random
 * and default is desired).
 *   	Once all senders are created and initialized, a central main-clock issues
 * a "clock-tick" which unblocks all threads once. Each thread executes it's
 * sequence which was defined (can be empty as well). The threads are theoretically
 * executed in parallel, but technically there are context-switching and core-switching
 * delays, to the synchronization precision at the beginning of each clock-tick
 * between threads is probably a few us, depending on the system. A thread sequence
 * for a clock-tick can contain several sending frames, they have to specified with a
 * sequence number. The time-interval between main-clock ticks can be configured, like
 * this the "speed can be cranked up easily" for testing purposes. Default is 100ms.
 *
 * For things to work properly two constraints have to be observed:
 * (1) the maximum execution time for a command sequence for any clock-tick must not
 *     exceed the fixed-limit of 10ms. Keep the sequences rather short therefore.
 *     Some additional diagnostics is implemented to ensure (and warn about) this.
 * (2) the main-clock will always run slower than the fixed-limit in order to allow
 *     enough time for thread and context switches. In practice:
 *     main-clock >= 2 * fixed-limit, giving a max speed of main-clock of 20ms / 50Hz.
 *     We can go slower of course, but not faster, otherwise we would loose
 *     thread-parallelism and the first thread would execute all clock-ticks because
 *     it would not free enough CPU.
 * Even though this is a bit tricky, both constraints are easy to keep in the context
 * of CAN testing for fast-paced thread switching.
 * Can also just send one single message without any threads, and exit.
 */
class CONNECTION : public CanxThreads {

public:
	CONNECTION();
	virtual ~CONNECTION();

	static vector<CONNECTION *> objSenderv;
	static bool shutup;
	static CONNECTION * getObjPtr( unsigned int i );
	static void allSendersShutup( void ){ CONNECTION::shutup = true; };

	void initialize( CanxThreads::ThreadData_t td );
	void storeObjPtr( void );
	void clearFrames( void ){ _thread_frames.clear(); };
	void showConfig( void );

	bool executeClockTick( unsigned int c );
	void executeStartTick( void );
	int getTotalTicksSender( void );
	void setGeneratedTicksSender( int t ) { _nbGeneratedTicksToExecute = t; };
	void addFrame( CanThreadFrame_t f );
	CanxThreads::CanThreadFrame_t generateRandomFrame( void );
	CanxThreads::CanThreadFrame_t generateCountingFrame( void );
	CanxThreads::CanThreadFrame_t generateFixedFrame( void );
	void setRepeat( bool rr ){ _repeatSequence = rr; };
	void resetSequence( void );
	void setGlobalTickTime( int tt ) { _globalTickTime = tt; };
	bool senderEnabled( void ){ return(_sender_enabled); }
	bool receiverEnabled( void ){ return(_receiver_enabled); }

	void enableSender( bool f ){ _sender_enabled = f; }
	void enableReceiver( bool f ){ _receiver_enabled = f; }

	static void onMessageRcvdCommon( uint32_t iobj, const CanMsgStruct/*&*/ message );
	static void onMessageRcvd0(const CanMsgStruct/*&*/ message); // reception handler can0...can15
	static void onMessageRcvd1(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd2(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd3(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd4(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd5(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd6(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd7(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd8(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd9(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd10(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd11(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd12(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd13(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd14(const CanMsgStruct/*&*/ message);
	static void onMessageRcvd15(const CanMsgStruct/*&*/ message);

	int totalMessagesSent( void ){ return( _totalMessagesSent ); }
	int totalMessagesReceived( void ){ return( _totalMessagesReceived ); }

	typedef struct {
		CanMsgStruct can;
		string time;
	} RECORD_t;
	void storeRecordingToFile( void );

private:
	static int _instanceCounter;
	static string _synchronizationMessage;
	// static int _totalMessageCounter;

	CanModule::CanLibLoader *_libloader;
	CanModule::CCanAccess *_cca;
	CanModule::CCanAccess *_ccaMirror;
	unsigned long int _messageCounterSequence;
	unsigned long int _messageCounterClockTick;
	int _delay_us;
	string _message;
	int _instance;
	int _clock;
	int _previousClock;
	int _nbGeneratedTicksToExecute;
	bool _repeatSequence;
	int _globalTickTime;
	bool _sender_enabled;
	bool _receiver_enabled;

	int _totalMessagesSent;
	int _totalMessagesReceived;
	RECORD_t _rec;

#ifdef _WIN32
	SYSTEMTIME _now, _t1, _tv;
	SYSTEMTIME _dstart, _dstop;
#else
	struct timeval _now, _t1, _dstart, _dstop, _tv;
	struct timezone _tz;
#endif
	double _delta;
	double _measDelay;
	double _timeoutValue;
	unsigned long int _frameCount;
	vector<CanxThreads::CanThreadFrame_t> _thread_frames;
	unsigned int _thread_frames_index;    // below this : they are already played
	std::vector<RECORD_t> _recording; // msg, time
	string _stringFromCanMsgStruct( CanMsgStruct &cm );

	void _deinitCAN( string port );
	bool _timeout( void );
	bool _timeout( double tt_ms ); // set - reset
	void _measureDelayStart( void );
	double _measureDelayStop( void );
	CanThreadFrame_t _nextSequenceFrame( void );
	int _nbFramesPerTick( void );
	string _gen_random_string( const unsigned int len );
	CanMessage _defaultCanMsg( void );
	CanMessage _standardCanMsgFromFrame( CanThreadFrame_t frame );
	uint8_t _stringToUChar( string s );
	int64_t _stringToLongInt( string s );
	void _initCANSystec( string name, string parameters );
	void _initCANPeak( string port, string parameters );
	void _initCANAnagate( string name, string parameters );
	void _processReceivedMessage( const CanMsgStruct msg );
	string _CanMsgToString( const CanMsgStruct message );

};

} // namespace sender_ns

#endif /* SENDER_H_ */
