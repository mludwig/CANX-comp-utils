/*
 * SpecialCommands.h
 *
 *  Created on: Feb 21, 2019
 *      Author: mludwig
 */

#ifndef SRC_SPECIALCOMMANDS_H_
#define SRC_SPECIALCOMMANDS_H_

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#ifdef _WIN32
	#include "AnaGateDllCan.h"
	#include "AnaGateDll.h"
	#include "tchar.h"
	#include "Winsock2.h"
	#include "windows.h"
#else
	#include "AnaGateDLL.h"
	#include "AnaGateDllCan.h"
	typedef unsigned long DWORD;
#endif


namespace SpecialCommands_ns {

class SpecialCommands {
public:
	SpecialCommands();
	virtual ~SpecialCommands();

	AnaInt32 anagateSoftwareReset( std::string ip );
};

} /* namespace SpecialCommands_ns */

#endif /* SRC_SPECIALCOMMANDS_H_ */
