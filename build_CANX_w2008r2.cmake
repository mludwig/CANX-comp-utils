# toolchain for w2008r2 CANX
#
# dependencies:
# CanModule
# 	LogIt
# 	boost
# boost
# (LogIt)
# should be built by jenkins in a parallel workspace on the same machine (physical i.e. pcitco067)
#
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")

# CanModule lib and headers
IF ( EXISTS $ENV{CANMODULE_PATH_LIBS} )
 	SET ( CANMODULE_PATH_LIBS $ENV{CANMODULE_PATH_LIBS} )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take environment var for CANMODULE_PATH_LIBS= ${CANMODULE_PATH_LIBS}")
ELSE()
	#SET( CANMODULE_PATH_LIBS "/xxx/lib" )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take default var for CANMODULE_PATH_LIBS= ${CANMODULE_PATH_LIBS}")
ENDIF()
IF ( EXISTS $ENV{CANMODULE_LIBS} )
 	SET ( CANMODULE_LIBS $ENV{CANMODULE_LIBS} )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take environment var for CANMODULE_LIBS= ${CANMODULE_LIBS}")
ELSE()
	SET( CANMODULE_LIBS "-lCanModule" )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take default var for CANMODULE_LIBS= ${CANMODULE_LIBS}")
ENDIF()
IF ( EXISTS $ENV{CANMODULE_HEADERS} )
	SET ( CANMODULE_HEADERS $ENV{CANMODULE_HEADERS} )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take environment var for CANMODULE_HEADERS= ${CANMODULE_HEADERS}")
ELSE()
	#SET ( CANMODULE_HEADERS "../CanInterface/include" )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take default var for CANMODULE_HEADERS= ${CANMODULE_HEADERS}")
ENDIF()
include_directories ( ${CANMODULE_HEADERS} )

# boost
IF ( EXISTS $ENV{BOOST_PATH_LIBS} )
 	SET ( BOOST_PATH_LIBS $ENV{BOOST_PATH_LIBS} )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take environment var for BOOST_PATH_LIBS= ${BOOST_PATH_LIBS}")
ELSE()
 	#SET ( BOOST_PATH_LIBS "/opt/boost/stage/lib" )
    #message("file [${CMAKE_CURRENT_LIST_FILE}]: take default var for BOOST_PATH_LIBS= ${BOOST_PATH_LIBS}")
ENDIF()
IF ( EXISTS $ENV{BOOST_HEADERS})
	SET ( BOOST_HEADERS $ENV{BOOST_HEADERS} )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take environment var for BOOST_HEADERS= ${BOOST_HEADERS}")
ELSE()
	#SET ( BOOST_HEADERS "/opt/boost" )
    #message("file [${CMAKE_CURRENT_LIST_FILE}]: take default var for BOOST_HEADERS= ${BOOST_HEADERS}")
ENDIF()

SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_program_options 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )
include_directories ( ${BOOST_HEADERS} )

# LogIt
IF ( EXISTS $ENV{LOGIT_HEADERS} )
	SET ( LOGIT_HEADERS $ENV{LOGIT_HEADERS} )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take environment var for LOGIT_HEADERS= ${LOGIT_HEADERS}")
ELSE()
	SET ( LOGIT_HEADERS "../LogIt/include" )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take default var for LOGIT_HEADERS= ${LOGIT_HEADERS}")
ENDIF()
IF ( EXISTS $ENV{LOGIT_PATH_LIBS} )
	SET ( LOGIT_PATH_LIBS $ENV{LOGIT_PATH_LIBS} )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take environment var for LOGIT_PATH_LIBS= ${LOGIT_PATH_LIBS}")
ELSE()
	SET ( LOGIT_PATH_LIBS "LogIt/Release" )
    message("file [${CMAKE_CURRENT_LIST_FILE}]: take default var for LOGIT_PATH_LIBS= ${LOGIT_PATH_LIBS}")
ENDIF()
include_directories ( ${LOGIT_HEADERS} )

# SET ( LOGIT_LIBS "LogIt.lib" )
# need static lib for windows as it seems
# SET ( LOGIT_LIBS "-lLogIt" )


# force paths for ci: jenkins inject env variables for windows does not work
SET ( CANMODULE_PATH_LIBS "C:/Jenkins/workspace/Can-Module.w2008r2-physical/bin/Release" "C:/Jenkins/workspace/Can-Module.w2008r2-physical/Release" )
SET ( CANMODULE_HEADERS "C:/Jenkins/workspace/Can-Module.w2008r2-physical/CanInterface/include" )
SET ( BOOST_PATH_LIBS "C:/Jenkins/workspace/boost_1_59_0_vanilla_w2008r2-physical/stage/lib" )
SET ( BOOST_HEADERS   "C:/Jenkins/workspace/boost_1_59_0_vanilla_w2008r2-physical" )

#SET ( SOCKETCAN_PATH_LIBS "not_used" )
#SET ( SOCKETCAN_HEADERS "not_used" )
#SET ( LOGIT_HEADERS "../LogIt/include" )

# vendor libs for windows...



